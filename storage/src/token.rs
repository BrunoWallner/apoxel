use common::Token;
use common::data::{deserialize, serialize};
use directories::ProjectDirs;
use std::fs::{self, File};
use std::io::{Read, Write};


pub fn get() -> Option<Token> {
    let project_dirs = ProjectDirs::from("org", "BrunoWallner", "Apoxel")?;
    let data_dir = project_dirs.data_dir();
    let token_dir = data_dir.join("token.dat");

    let mut buffer: Token = [0_u8; 16];
    let mut file = File::open(token_dir).ok()?;
    file.read_exact(&mut buffer).ok()?;

    let token: Token = deserialize(&buffer)?;
    Some(token)
}

pub fn set(token: &Token) -> Result<(), ()> {
    let project_dirs = ProjectDirs::from("org", "BrunoWallner", "Apoxel").ok_or(())?;
    let data_dir = project_dirs.data_dir();
    trim_result(crate::create_directory(data_dir))?;

    let token_dir = data_dir.join("token.dat");
    let token = serialize(token).ok_or(())?;

    let mut file = File::create(token_dir).unwrap();
    trim_result(file.write_all(&token))?;

    Ok(())
}

pub fn remove() -> Result<(), ()> {
    let project_dirs = ProjectDirs::from("org", "BrunoWallner", "Apoxel").ok_or(())?;
    let data_dir = project_dirs.data_dir();
    let token_dir = data_dir.join("token.dat");
    trim_result(fs::remove_file(token_dir))?;

    Ok(())
}

pub fn trim_result<T, E>(result: Result<T, E>) -> Result<T, ()> {
    match result {
        Ok(t) => Ok(t),
        Err(_) => Err(()),
    }
}