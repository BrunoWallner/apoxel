use common::blocks::Block;
use common::chunk::SuperChunk;
use common::prelude::Color;
use common::types::Octree;

const RESOLUTION: i64 = 32;

pub static mut CACTI: Vec<SuperChunk> = Vec::new();

pub fn get_cactus(t: f32) -> SuperChunk {
    unsafe { CACTI[((CACTI.len() - 1) as f32 * t) as usize].clone() }
}

pub fn generate(_seed: u32) {
    let mut chunk = SuperChunk::new(Octree::default(), [0, 0, 0]);

    let half = RESOLUTION / 2;
    let coords: Vec<[i64; 3]> = vec![
        [half, 0, half],
        [half, 1, half],
        [half, 2, half],
        [half - 1, 2, half],
        [half + 1, 2, half],
        [half - 2, 2, half],
        [half + 2, 2, half],
        [half - 2, 3, half],
        [half + 2, 3, half],
        [half, 3, half],
        [half, 4, half],
    ];

    for coord in coords {
        chunk.place(
            RESOLUTION as u32,
            coord,
            Block::Color(Color::new(200, 20, 20)),
        );
    }

    unsafe { CACTI.push(chunk) };
}
