use common::blocks::Block;
use common::chunk::SuperChunk;
use common::prelude::Color;
use common::types::Octree;

fn build_sphere(radius: i64, blocks: &[Block], seed: u32) -> SuperChunk {
    let noise = noise::Noise::new(seed);
    let mut sphere = SuperChunk::new(Octree::default(), [0, 0, 0]);
    for x in (RESOLUTION / 2 - radius)..(RESOLUTION / 2 + radius) {
        for y in (RESOLUTION / 2 - radius)..(RESOLUTION / 2 + radius) {
            for z in (RESOLUTION / 2 - radius)..(RESOLUTION / 2 + radius) {
                let val = noise.get_3d([x as f64, y as f64, z as f64], 0.15);
                let block = blocks[((blocks.len() - 1) as f64 * val) as usize];
                if block != Block::None {
                    if common::calculate_chunk_distance(
                        &[x, y, z],
                        &[RESOLUTION / 2, RESOLUTION / 2, RESOLUTION / 2],
                    ) < radius
                    {
                        sphere.place(RESOLUTION as u32, [x, y, z], block);
                    }
                }
            }
        }
    }
    sphere
}

use crate::world::noise;

const RESOLUTION: i64 = 64;

const AMOUNT: usize = 10;

pub static mut TREES: Vec<SuperChunk> = Vec::new();

pub fn get_tree(t: f32) -> SuperChunk {
    unsafe { TREES[((TREES.len() - 1) as f32 * t) as usize].clone() }
}

pub fn generate(mut seed: u32) {
    for _ in 0..AMOUNT {
        let mut chunk = SuperChunk::new(Octree::default(), [0, 0, 0]);

        let cross = build_log(&mut chunk, seed);
        build_leaves(&mut chunk, &cross, seed);

        seed += 806034;
        seed /= 2;

        unsafe { TREES.push(chunk) }
    }
}

const DISPLACEMENT: f64 = 0.03;
fn build_log(chunk: &mut SuperChunk, seed: u32) -> [f64; 2] {
    let noise = noise::Noise::new(seed);

    let radius = noise.get_2d([0.0, 0.0], 1.0).clamp(0.2, 1.0) * 5.0;
    let sphere = build_sphere(
        radius as i64,
        &[Block::Color(Color::new(30, 200, 50)), Block::None],
        seed,
    );

    let height = RESOLUTION / 2;

    let mut cross = [0.5, 0.5];

    for y in 0..height {
        // chunk.place(RESOLUTION as u32, [(cross[0] * RESOLUTION as f64) as i64, y as i64, (cross[0] * RESOLUTION as f64) as i64], Block::OakWood);
        chunk.merge(
            &sphere,
            [
                cross[0] as f32,
                y as f32 / RESOLUTION as f32,
                cross[1] as f32,
            ],
            Some(RESOLUTION as u32),
        );
        cross[0] += noise.get_2d(cross, 0.0001) * DISPLACEMENT;
        cross[1] += noise.get_2d(cross, 0.0001) * DISPLACEMENT;
    }

    cross
}

const LEAVE_DISPLACEMENT: f64 = 0.1;

fn build_leaves(chunk: &mut SuperChunk, cross: &[f64; 2], seed: u32) {
    let noise = noise::Noise::new(seed);
    let sphere = build_sphere(
        10,
        &[
            Block::Color(Color::new(30, 200, 50)),
            Block::None,
            Block::None,
        ],
        seed,
    );
    let coord = [cross[0], 0.45, cross[1]];

    let mut noise_coord = coord.clone();
    for _ in 0..=5 {
        let mut c = coord.clone();
        c[0] += (noise.get_3d(noise_coord, 0.0001) * LEAVE_DISPLACEMENT * 2.0)
            .clamp(-LEAVE_DISPLACEMENT, LEAVE_DISPLACEMENT);
        c[1] += (noise.get_3d(noise_coord, 0.0001) * LEAVE_DISPLACEMENT * 2.0)
            .clamp(-LEAVE_DISPLACEMENT, LEAVE_DISPLACEMENT);
        c[2] += (noise.get_3d(noise_coord, 0.0001) * LEAVE_DISPLACEMENT * 2.0)
            .clamp(-LEAVE_DISPLACEMENT, LEAVE_DISPLACEMENT);

        noise_coord = c;

        chunk.merge(
            &sphere,
            [c[0] as f32, c[1] as f32, c[2] as f32],
            Some(RESOLUTION as u32),
        );
    }
}
