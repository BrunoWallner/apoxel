use common::{Coord, Coord2D};
use noise::{core::worley::ReturnType, Billow, MultiFractal, NoiseFn, OpenSimplex, Worley};

const BIOME_OCTAVES: usize = 10;
const TERRAIN_OCTAVES: usize = 10;
const CAVE_OCTAVES: usize = 8;
const CAVE_SIZE: f64 = 50.0;
const CAVE_PROBABILITY: f64 = 0.2;

pub struct Noise {
    simplex: OpenSimplex,
}
impl Noise {
    pub fn new(seed: u32) -> Self {
        let simplex = OpenSimplex::new(seed);
        Self { simplex }
    }

    pub fn get_3d(&self, coord: [f64; 3], size: f64) -> f64 {
        let noise_coord = [coord[0] / size, coord[1] / size, coord[2] / size];
        let out = self.simplex.get(noise_coord);

        out
    }

    pub fn get_2d(&self, coord: [f64; 2], size: f64) -> f64 {
        let noise_coord = [coord[0] / size, coord[1] / size];
        let out = self.simplex.get(noise_coord);

        out
    }
}

pub struct CaveNoise {
    simplex: Billow<OpenSimplex>,
}
impl CaveNoise {
    pub fn new(seed: u32) -> Self {
        let simplex = Billow::new(seed).set_octaves(CAVE_OCTAVES);
        Self { simplex }
    }

    pub fn is_cave(&self, coord: Coord) -> bool {
        let noise_coord = [
            coord[0] as f64 / CAVE_SIZE,
            coord[1] as f64 / CAVE_SIZE,
            coord[2] as f64 / CAVE_SIZE,
        ];

        let f = self.simplex.get(noise_coord) + 2.0;
        f <= CAVE_PROBABILITY
    }
}

pub struct BiomeNoise {
    worley: Worley,
    simplex: Billow<OpenSimplex>,
}
impl BiomeNoise {
    pub fn new(seed: u32) -> Self {
        let worley = Worley::new(seed).set_return_type(ReturnType::Distance);
        let simplex = Billow::new(seed).set_octaves(BIOME_OCTAVES);
        Self { worley, simplex }
    }

    pub fn get_type(&self, coord: Coord2D, size: f64) -> f64 {
        let noise_coord = [coord[0] as f64 / size, coord[1] as f64 / size];

        let worley = self.worley.get(noise_coord) + 0.5;
        // invert
        let worley = worley * -1.0 + 1.0;
        let worley = worley.clamp(0.0, 1.0);

        // fractal simplex noise
        let simplex = (self.simplex.get(noise_coord) + 2.0).clamp(0.0, 2.0);

        let out = (worley + simplex) / 2.0;
        out
    }
}

pub struct TerrainNoise {
    simplex: Billow<OpenSimplex>,
}
impl TerrainNoise {
    pub fn new(seed: u32) -> Self {
        let simplex = Billow::new(seed).set_octaves(TERRAIN_OCTAVES);
        Self { simplex }
    }

    pub fn get_height(&self, coord: Coord2D, size: f64) -> f64 {
        let noise_coord = [coord[0] as f64 / size, coord[1] as f64 / size];
        let simplex = (self.simplex.get(noise_coord) + 2.0).clamp(0.0, 2.0);
        simplex
    }
}
