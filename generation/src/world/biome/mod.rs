// const BIOME_SIZE: f64 = 400.0;
// const TERRAIN_SIZE: f64 = 40.0;

// use super::noise::{BiomeNoise, TerrainNoise};
// use common::blocks::Block;
// use common::color::Color;
// use common::Coord2D;

// pub struct BiomeOutput {
//     pub height: i64,
//     pub blocks: Vec<Block>,
// }

// const BIOME_TYPE_CUT: f64 = 0.65;
// enum BiomeType {
//     Ocean,
//     Land,
// }
// impl BiomeType {
//     pub fn get(x: f64) -> Self {
//         match x {
//             x if x <= BIOME_TYPE_CUT => Self::Ocean,
//             x if x >= BIOME_TYPE_CUT => Self::Land,
//             _ => Self::Ocean,
//         }
//     }
// }

// pub fn get(coord: Coord2D, biome_noise: &BiomeNoise, terrain_noise: &TerrainNoise) -> BiomeOutput {
//     let biome_type_value = biome_noise.get_type(coord, BIOME_SIZE);
//     let biome_type = BiomeType::get(biome_type_value);
//     let biome_certainty: f64 =
//         (biome_type_value.clamp(BIOME_TYPE_CUT, 1.0) - BIOME_TYPE_CUT) / BIOME_TYPE_CUT * 2.0;

//     let (start_height, end_height) = match biome_type {
//         BiomeType::Ocean => (0.0, 0.0),
//         BiomeType::Land => (1.0, terrain_noise.get_height(coord, TERRAIN_SIZE) * 20.0),
//     };

//     let height = (start_height * (1.0 - biome_certainty)) + (end_height * biome_certainty);
//     let height = height as i64;

//     let block = match biome_type {
//         BiomeType::Ocean => Block::Water,
//         BiomeType::Land => {
//             if biome_certainty > 0.8 {
//                 // rock
//                 Block::Color(Color::new(100, 100, 100))
//             } else if biome_certainty > 0.2 {
//                 // grass
//                 Block::Color(Color::new(5, 150, 10))
//             } else {
//                 // sand
//                 Block::Color(Color::new(194, 178, 128))
//             }
//         }
//     };

//     BiomeOutput {
//         height,
//         blocks: vec![block],
//     }
// }
