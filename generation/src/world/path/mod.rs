use common::Coord2D;
use std::collections::BTreeMap;

const RESOLUTION_PER_CHUNK: usize = 16;

#[derive(Copy, Clone, Debug)]
pub struct PathGrid {
    grid: [bool; RESOLUTION_PER_CHUNK * RESOLUTION_PER_CHUNK],
}
impl PathGrid {
    pub fn new() -> Self {
        Self {
            grid: [false; RESOLUTION_PER_CHUNK * RESOLUTION_PER_CHUNK],
        }
    }

    pub fn set(&mut self, x: usize, y: usize) {
        let index = x + (y * RESOLUTION_PER_CHUNK);
        self.grid[index] = true;
    }

    pub fn unset(&mut self, x: usize, y: usize) {
        let index = x + (y * RESOLUTION_PER_CHUNK);
        self.grid[index] = false;
    }

    pub fn is_set(&self, x: usize, y: usize) -> bool {
        let index = x + (y * RESOLUTION_PER_CHUNK);
        self.grid[index]
    }
}

pub struct PathMap {
    map: BTreeMap<Coord2D, PathGrid>,
}
