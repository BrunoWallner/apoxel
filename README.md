# Apoxel
Ray traced voxels

An octree raytracer can be found at `path`, it can render 512³ octrees with ease

A rasterized client using the bevy game engine can be found at `client`

![path](./media/path.png)

## How to run
1. install rust via [rustup](https://rustup.rs/) or your distro's package manager
2. cd into `path` and run it via `cargo run --release`.

In the `Load world` window you can select your minecraft's `region` folder of your world
and import it, or let a noise world generate.

Press the left mouse button to destroy blocks, or press the right mouse button to place light
