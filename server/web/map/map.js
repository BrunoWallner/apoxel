const TEXTURE_SIZE = 32;
let map_zoom = 16;

let updated_since_last_render = true;
let tiles = new Map();
let map_position = [0.0, 0.0];

let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");
ctx.imageSmoothingEnabled = false;

setInterval(renderMap, 500);

// first 2 64 bit integers are coords, the rest is all rgb data
function addTileFromBytes(bytes) {
  updated_since_last_render = true;
 let x = bytes[0] - 128;
  let y = bytes[1] - 128;
  
  let coord = [x, y];
    
  // allocate new Uint8ClampedArray with alpha values of 255
  const ARRAY_SIZE = TEXTURE_SIZE * TEXTURE_SIZE;
  let rgba = new Uint8ClampedArray(ARRAY_SIZE * 4);
  rgba.fill(255);

  const BYTES_COORD_OFFSET = 2;
  for (let index = 0; index < ARRAY_SIZE; index += 1) {
    let rgb_index = index * 3 + BYTES_COORD_OFFSET;
    let rgba_index = index * 4;
    
    for (let rgb_offset = 0; rgb_offset < 3; rgb_offset += 1) {
      // console.log(rgb);
      // rgba[rgba_index + rgb] = bytes[rgb_index + rgb];
      // rgba[rgba_index + rgb] = 100;
      let rgb = bytes[rgb_index + rgb_offset];
      rgba[rgba_index + rgb_offset] = rgb;
    }
  }
  
  let palette = new ImageData(rgba, TEXTURE_SIZE, TEXTURE_SIZE);
  tiles.set(coord,  palette); 
}

function loadMap(x, y) {
  let radius = parseInt(map_zoom / 2);
  let string = "load" + " " + x + " " + y + " " + radius;
  send_message(string)
}

function renderMap() {
  if (updated_since_last_render) {
    let canvas_size = TEXTURE_SIZE * map_zoom;
    canvas.width = canvas_size;
    canvas.height = canvas_size;

    updated_since_last_render = false;
    // ctx.clearRect(0, 0, canvas_size, canvas_size);

    tiles.forEach((tile, coord) => {
      let image_coord = [
        coord[0] * TEXTURE_SIZE + canvas_size / 2 - map_position[0] * TEXTURE_SIZE,
        coord[1] * TEXTURE_SIZE + canvas_size / 2 - map_position[1] * TEXTURE_SIZE,
      ];
      
      ctx.putImageData(tile, image_coord[0], image_coord[1]);
    });
  }
}