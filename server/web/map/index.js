/* compilation on server */
/* compiles to: */
// let socket = new WebSocket(HOST_IP);

/* @COMPILE: socket */

let socket_is_available = false;

socket.onopen = function() {
  socket_is_available = true;
  console.log("[CONNECTED]");
  setInterval(function() {socket.send("cycle")}, 500);
}

socket.onmessage = function(message) {
  let blob = message.data;
  // holy fucking shit JS is such a horrible clusterfuck
  new Response(blob).arrayBuffer()
  .then(function(array_buffer) {
    let bytes = new Uint8ClampedArray(array_buffer);
    addTileFromBytes(bytes);
  });
}

function send_message(msg) {
  if (socket_is_available) {
    socket.send(msg);
  } else {
    console.warn("tried to send message while socket is in invalid state");
  }
}

let slider = document.getElementById("map_zoom_slider");
slider.oninput = function() {
  map_zoom = this.value;
  
  updated_since_last_render = true;
  renderMap();  
}

/* functions for buttons in index.html */
function load() {
  let x = document.getElementById("x_input").value;
  let y = document.getElementById("y_input").value;
  
  loadMap(x, y, 10);
}

function move() {
  let x = document.getElementById("x_input").value;
  let y = document.getElementById("y_input").value;
  
  map_position = [x, y];
  
  updated_since_last_render = true;
  renderMap();  
}
