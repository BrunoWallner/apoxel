use std::str::FromStr;

pub enum Param {
    Command(Command),
    Target(Target),
}
impl FromStr for Param {
    type Err = ();

    #[rustfmt::skip]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(command) = s.parse::<Command>() {return Ok(Self::Command(command))};
        if let Ok(target) = s.parse::<Target>() {return Ok(Self::Target(target))};

        Err(())
    }
}

pub enum Command {
    Shutdown,
    Clear,
    Help,
    Status,
    Kill,
}
impl FromStr for Command {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "shutdown" => Ok(Self::Shutdown),
            "clear" => Ok(Self::Clear),
            "help" => Ok(Self::Help),
            "status" => Ok(Self::Status),
            "kill" => Ok(Self::Kill),
            _ => Err(()),
        }
    }
}

pub enum Target {
    Everyone,
    Everything,
    Player(String),
}
impl FromStr for Target {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "@a" => Ok(Self::Everyone),
            "@e" => Ok(Self::Everything),
            _ => {
                // if s.starts_with('@') {
                // Ok(Self::Player(String::from(&s[1..])))
                // } else {
                // Err(())
                // }
                let Some(stripped) = s.strip_prefix('@') else {return Err(())};
                Ok(Self::Player(String::from(stripped)))
            }
        }
    }
}

pub struct ParameterSet {
    pub params: Vec<Param>,
}
impl FromStr for ParameterSet {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split = s.split_whitespace().collect::<Vec<&str>>();
        let mut params = Vec::with_capacity(split.len());
        for split in split {
            let Ok(param) = split.parse::<Param>() else {return Err(())};
            params.push(param);
        }
        Ok(Self { params })
    }
}

// use colored::*;

// pub enum CommandType {
//     Shutdown,
//     Clear,
//     Help,
//     Status,
//     Kill,
// }
// impl<'a> CommandType {
//     pub fn get_params(&self) -> &'a [&'a [&'a str]] {
//         match self {
//             Self::Kill =>
//         }
//     }
// }
// pub enum Command {
//     Shutdown,
//     Clear,
//     Help,
//     Status,
//     Kill(Argument),
// }

// pub enum Argument {
//     Everyone,
//     Everything,
//     Player(String),
// }

// fn test() {
//     let command = Command::Variant(Command::Shutdown);
// }

// #[derive(Copy, Clone, Debug, PartialEq)]
// pub enum ParamType {
//     Command,
//     Target,
// }
// impl ParamType {
//     pub fn values(&self) -> Vec<String> {
//         match self {
//             ParamType::Command => Command::all().into_iter().map(|x| x.to_string()).collect(),
//             _ => Vec::new(),
//         }
//     }
// }

// pub struct Command {
//     param_type: ParamType,

// }

// // pub trait Param<'a> {
// //     fn to_string(&self) -> String;
// //     fn parse(str: &str) -> Option<Self>
// //     where
// //         Self: Sized;
// //     fn fmt(&self) -> String;
// //     fn arguments(&self) -> Vec<ParamType>;
// //     fn all() -> &'a [Self]
// //     where
// //         Self: Sized;
// // }

// // #[derive(Clone, Debug, PartialEq)]
// // pub enum Command {
// //     Shutdown,
// //     Clear,
// //     Help,
// //     Status,
// // }

// // impl<'a> Param<'a> for Command {
// //     fn to_string(&self) -> String {
// //         use Command::*;
// //         let str = match self {
// //             Shutdown => "shutdown",
// //             Clear => "clear",
// //             Help => "help",
// //             Status => "status",
// //         };
// //         String::from(str)
// //     }

// //     fn parse(str: &str) -> Option<Self>
// //     where
// //         Self: Sized,
// //     {
// //         use Command::*;
// //         match str {
// //             "shutdown" => Some(Shutdown),
// //             "clear" => Some(Clear),
// //             "help" => Some(Help),
// //             "status" => Some(Status),
// //             _ => None,
// //         }
// //     }

// //     fn fmt(&self) -> String {
// //         self.to_string().color(Color::Green).to_string()
// //     }

// //     fn arguments(&self) -> Vec<ParamType> {
// //         // currently no command with param
// //         match self {
// //             _ => Vec::with_capacity(0),
// //         }
// //     }

// //     fn all() -> &'a [Self] {
// //         use Command::*;
// //         &[Shutdown, Clear, Help, Status]
// //     }
// // }
