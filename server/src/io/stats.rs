use crate::allocator::allocated;

#[derive(Clone, Debug)]
pub struct Stats {
    pub allocated_bytes: usize,
}

impl Stats {
    pub fn get() -> Self {
        let allocated_bytes = allocated();

        Self { allocated_bytes }
    }

    pub fn fmt(&self) -> String {
        let string = format!(
            "{}: {} MB",
            "allocated_bytes",
            self.allocated_bytes / 1_000_000
        );

        string
    }
}
