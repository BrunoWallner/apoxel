use super::commands::{Command, Param, ParameterSet};
use super::stats::Stats;

use crossterm::event::KeyCode;
use crossterm::event::KeyEvent;
use crossterm::event::KeyModifiers;
use crossterm::{event, terminal};

use colored::*;

use crate::logger::LogEvent;
use common::channel::*;

use crossterm::{
    cursor::{Hide, MoveTo, Show},
    execute, queue,
    terminal::{
        disable_raw_mode, enable_raw_mode, Clear, ClearType, EnterAlternateScreen,
        LeaveAlternateScreen,
    },
};
use std::io::{stdout, Result as IoResult, Write};
use std::thread;
use std::time::Duration;

const TERMINAL_IO_TIMEOUT: Duration = Duration::from_millis(50);

#[derive(Clone, Debug)]
enum TerminalEvent {
    Key(KeyEvent),
    Resize,
}

pub struct Terminal {
    messages: Vec<String>,
    event_receiver: Receiver<TerminalEvent>,
    event_sender: Sender<TerminalEvent>,
    command_buffer: String,
}

impl Terminal {
    pub fn new() -> IoResult<Self> {
        let mut stdout = stdout();

        let (event_sender, event_receiver) = channel(None);

        queue!(stdout, EnterAlternateScreen, Hide)?;
        enable_raw_mode()?;
        stdout.flush()?;

        Ok(Self {
            event_receiver,
            event_sender,
            command_buffer: String::new(),
            messages: Vec::new(),
        })
    }
    pub fn run(&mut self, log_event_receiver: &Receiver<LogEvent>) {
        Terminal::init_event_fetcher(self.event_sender.clone());

        'running: loop {
            if let Some(event) = self.event_receiver.recv_timeout(TERMINAL_IO_TIMEOUT) {
                match event {
                    TerminalEvent::Resize => {
                        let _ = self.clear();
                    }
                    TerminalEvent::Key(key) => {
                        if key.code == KeyCode::Char('c') && key.modifiers == KeyModifiers::CONTROL
                        {
                            break 'running;
                        }
                        if let KeyCode::Char(c) = key.code {
                            self.command_buffer.push(c)
                        }
                        // submit command buffer
                        if key.code == KeyCode::Enter {
                            if let Ok(parameter) = &self.command_buffer.parse::<ParameterSet>() {
                                if self.execute_command(parameter) {
                                    break 'running;
                                }
                            } else {
                                log::warn!(
                                    "invalid command: {}",
                                    self.command_buffer.color(Color::BrightRed)
                                );
                            }
                            self.command_buffer.clear();
                        }
                        if key.code == KeyCode::Backspace {
                            self.command_buffer.pop();
                        }
                    }
                }
            }

            while let Ok(message) = log_event_receiver.try_recv() {
                let formatted = message.fmt();
                self.push_message(formatted);
            }

            self.draw_messages();
            self.draw_command_buffer();
            stdout().flush().ok();
        }
    }

    fn execute_command(&mut self, params: &ParameterSet) -> bool {
        if let Param::Command(command) = &params.params[0] {
            match command {
                Command::Shutdown => return true,
                Command::Clear => {
                    let _ = self.clear();
                    self.messages.clear()
                }
                Command::Help => {
                    log::info!("fuck you")
                }
                Command::Status => {
                    let stats = Stats::get();
                    let fmt = stats.fmt();
                    log::info!("{}", fmt);
                }
                Command::Kill => (),
            }
        }

        false
    }

    pub fn exit(&self) {
        let _ = execute!(stdout(), LeaveAlternateScreen, Show);
        let _ = disable_raw_mode();
    }

    pub fn get_messages(&self) -> Vec<String> {
        self.messages.clone()
    }

    pub fn push_message(&mut self, message: String) {
        self.messages.push(message);
    }

    fn get_size(&mut self) -> IoResult<(u16, u16)> {
        terminal::size()
    }

    fn clear(&mut self) -> IoResult<()> {
        queue!(stdout(), Clear(ClearType::All), Clear(ClearType::Purge))
    }

    fn draw_messages(&mut self) {
        let Ok((width, height)) = self.get_size() else {return};
        // to add padding to command prompt
        let height = height - 2;

        let messages = self.get_messages();
        let message_height = messages.len();
        let start = if message_height > height as usize {
            message_height - height as usize
        } else {
            0
        };

        let message_slice = &messages[start..];

        for (y, message) in message_slice.iter().enumerate() {
            let mut line = String::from(message).replace('\n', " ");
            if line.len() < width as usize {
                let filler: String = (0..(width as usize - line.len())).map(|_| " ").collect();
                line = line + &filler;
            }
            queue!(stdout(), MoveTo(0, y as u16)).ok();
            write!(stdout(), "{line}").ok();
        }
        stdout().flush().ok();
    }

    fn draw_command_buffer(&mut self) {
        let Ok((width, height)) = self.get_size() else {return};
        let fmt = self.command_buffer.clone();
        let mut fmt = format!("{}: {}", "command".color(Color::Magenta), fmt);
        Self::fill_line(&mut fmt, width);

        queue!(stdout(), MoveTo(0, height)).ok();
        write!(stdout(), "{fmt}").ok();

        self.draw_code_completion();
    }

    fn draw_code_completion(&mut self) {
        // get last command

        // let param = command.params()[0];
        // let completion = <param as Param>::completions();
        // let param = <Box<dyn Param>>::completions();

        // let command_strings: Vec<String> = Command::completions().iter().map(|x| x.fmt()).collect();
        // let Some(box_width) = command_strings.iter().map(|x| x.len()).max() else {return};
        // let box_x = (self.command_buffer.len() as isize - 10).max(0) as u16;

        // for (y, mut completion) in command_strings.into_iter().enumerate() {
        //     let y = (height as i32 - y as i32).min(0) as u16;
        //     Self::fill_line(&mut completion, box_width as u16);
        //     queue!(stdout(), MoveTo(box_x, y)).ok();
        //     write!(stdout(), "{}", completion).ok();
        // }
    }

    fn fill_line(line: &mut String, width: u16) {
        if line.len() < width as usize {
            let filler: String = (0..(width as usize - line.len())).map(|_| " ").collect();
            *line += &filler;
        }
    }

    fn init_event_fetcher(sender: Sender<TerminalEvent>) {
        thread::spawn(move || loop {
            let Ok(event) = event::read() else {break};
            match event {
                event::Event::Key(event) => {
                    sender.send(TerminalEvent::Key(event)).ok();
                }
                event::Event::Resize(_, _) => {
                    sender.send(TerminalEvent::Resize).ok();
                }
                _ => (),
            }
        });
    }
}

impl Drop for Terminal {
    fn drop(&mut self) {
        self.exit();
    }
}
