pub mod chunk;
pub mod map;
pub mod users;

use common::channel::*;

#[derive(Clone, Debug)]
pub struct Hook<I, O> {
    pub input: Broadcast<I>,
    pub output: Broadcast<O>,
}
