use super::Hook;
use crate::map::Tile;
use common::prelude::*;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref MAP_HOOK: Hook<MapInputEvent, MapOutputEvent> = {
        let input = Broadcast::new();
        let output = Broadcast::new();
        Hook { input, output }
    };
}

#[derive(Clone, Debug)]
pub enum MapInputEvent {
    GetTiles {
        tiles: Vec<Coord2D>,
        sender: Sender<(Tile, Coord2D)>,
    },
}

#[derive(Clone, Debug)]
pub enum MapOutputEvent {
    TileUpdate { coord: Coord2D },
}
