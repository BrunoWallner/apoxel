mod allocator;
mod chunks;
mod client;
mod config;
mod hooks;
mod map;
mod shutdown;
mod tcp;
mod users;

use allocator::Allocator;
use async_std::task;
use colored::{Color, *};
use common::prelude::*;
pub use config::Config;
use log::*;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

#[global_allocator]
static ALLOCATOR: Allocator = Allocator;

#[macro_use]
extern crate lazy_static;

pub async fn start() -> Result<(), ()> {
    generation::foliage::generate(94969);

    if config::init().is_err() {
        log::error!(
            "[{}]: failed to load {}",
            "ERROR".color(Color::Red),
            "config.toml".color(Color::BrightMagenta)
        );
        return Err(());
    }

    let addr = SocketAddr::new(
        IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)),
        config::get().connection.port,
    );
    let tcp = tcp::Tcp::init(addr).await.unwrap();
    info!("server is listening on {}", addr);

    // init handles
    users::handle::init();
    chunks::handle::init();
    map::handle::init();
    map::web::init();

    // accepting clients
    task::spawn(async move {
        while let Ok(((output, input), addr)) = tcp.accept().await {
            task::spawn(async move {
                client::init(output, input, addr).await;
            });
        }
    });

    return Ok(());
}

pub fn shut_down() {
    let (_, shutdown_handle) = shutdown::get().unwrap();
    shutdown_handle.ignore();

    shutdown::shut_down();
    shutdown_handle.wait_until_safe();
}

pub fn allocated() -> usize {
    allocator::allocated()
}
