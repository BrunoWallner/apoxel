use crate::hooks::users::*;
use common::prelude::*;
use std::{collections::HashMap, thread};

pub fn init() {
    let user_hook = USER_HOOK.clone();
    let instruction_receiver = user_hook.input.get_receiver().unwrap();

    thread::spawn(move || {
        let mut users: HashMap<Token, User> = HashMap::default();
        while let Some(instruction) = instruction_receiver.recv() {
            match instruction {
                UserInputEvent::Register { name, token } => {
                    let mut already_exist: bool = false;
                    users.iter().for_each(|x| {
                        if x.1.name == name {
                            already_exist = true;
                        }
                    });
                    if !already_exist {
                        let (tok, user) = User::new(name);
                        users.insert(tok, user);
                        let _ = token.send(Some(tok));
                    } else {
                        let _ = token.send(None);
                    }
                }
                UserInputEvent::Login { token, success } => {
                    if let Some(user) = users.get_mut(&token) {
                        if !user.online {
                            user.online = true;
                            let _ = success.send(Ok(()));
                        } else {
                            let _ = success.send(Err(LoginError::AlreadyLoggedIn));
                        }
                    } else {
                        let _ = success.send(Err(LoginError::InvalidToken));
                    }
                }
                UserInputEvent::Logoff { token, success } => {
                    if let Some(user) = users.get_mut(&token) {
                        user.online = false;
                        let _ = success.send(true);
                    } else {
                        let _ = success.send(false);
                    }
                }
                UserInputEvent::GetUser { token, user } => {
                    if let Some(usr) = users.get(&token) {
                        let _ = user.send(Some(usr.clone()));
                    } else {
                        let _ = user.send(None);
                    }
                }
                UserInputEvent::ModUser {
                    token,
                    mod_instruction,
                } => {
                    if let Some(user) = users.get_mut(&token) {
                        match mod_instruction {
                            UserModInstruction::Move(coord) => user.pos = coord,
                        }
                    }
                }
            }
        }
    });
}
