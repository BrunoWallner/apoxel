#![forbid(unsafe_code)]

use std::thread;

use colored::*;
use common::channel::*;
use log::Level;
use log::LevelFilter;
use log::SetLoggerError;

#[derive(Clone, Debug)]
pub struct LogEvent {
    pub level: log::Level,
    pub message: String,
}

impl LogEvent {
    pub fn fmt(&self) -> String {
        let level_color = match self.level {
            Level::Error => Color::Red,
            Level::Warn => Color::Yellow,
            Level::Info => Color::Cyan,
            _ => Color::White,
        };

        let formatted = format!(
            "[{}]: {}",
            format!("{}", self.level).color(level_color),
            self.message
        );

        formatted
    }
}

#[derive(Clone, Debug)]
pub struct Logger {
    pub sender: Sender<LogEvent>,
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        let _ = self.sender.send(LogEvent {
            level: record.metadata().level(),
            message: format!("{}", record.args()),
        });
    }

    fn flush(&self) {}
}

#[allow(clippy::new_ret_no_self)]
impl Logger {
    pub fn new() -> Result<Receiver<LogEvent>, SetLoggerError> {
        let (sender, receiver) = channel(None);
        let logger = Logger { sender };

        log::set_boxed_logger(Box::new(logger))?;
        log::set_max_level(LevelFilter::Info);

        Ok(receiver)
    }
}

pub struct ShutdownLogger;

impl ShutdownLogger {
    pub fn run(log_event_receiver: &Receiver<LogEvent>) {
        loop {
            let Some(message) = log_event_receiver.recv() else {return};
            let formatted = message.fmt();
            println!("{formatted}");
        }
    }

    pub fn run_detached(log_event_receiver: Receiver<LogEvent>) {
        thread::spawn(move || ShutdownLogger::run(&log_event_receiver));
    }
}
