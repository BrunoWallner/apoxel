pub mod handle;
pub mod web;

use common::prelude::*;

const TERRAIN_START: i64 = 0;
const TERRAIN_END: i64 = 128;

const TEXTURE_SIZE: u32 = Octree::get_size_from_depth(5);

#[derive(Clone, Debug)]
pub struct Tile {
    pub texture: Vec<[u8; 3]>,
    pub size: usize,
}
impl Tile {
    pub fn new(size: usize) -> Self {
        Self {
            texture: vec![[0; 3]; size.pow(2)],
            size,
        }
    }

    pub fn merge(&mut self, other: &Self) {
        assert_eq!(self.size, other.size, "tiles have different sizes");

        for i in 0..self.size.pow(2) {
            let other = other.texture[i];
            if other != [0, 0, 0] {
                self.texture[i] = other
            }
        }
    }

    pub fn render(chunk: &Chunk, coord: Coord) -> Self {
        let (max_depth, output) = chunk.traverse(Some(5));
        let out_size = Octree::get_size_from_depth(max_depth);

        let mut height_map = vec![0_i64; TEXTURE_SIZE.pow(2) as usize];

        let mut tile = Tile::new(TEXTURE_SIZE as usize);
        for mut out in output {
            out.multiply_position(out_size as f32);
            // let size = Octree::get_size_from_depth(out.depth) as usize;
            let size = TEXTURE_SIZE / Octree::get_size_from_depth(out.depth);

            let position = out.position;
            let block = match out.node.to_end().0 {
                Some(b) => b,
                None => Block::None,
            };

            let x = position[0] as usize;
            let z = position[2] as usize;

            let chunk_height = BASE_CHUNK_SIZE as i64 * coord[1];
            let absolute_height = chunk_height + position[1] as i64;
            let color_multiplier = Self::get_relative_height(absolute_height);
            for x in x..x + size as usize {
                for z in z..z + size as usize {
                    let index = Self::convert_dimension(x, z);
                    if height_map[index] < position[1] as i64 {
                        let mut color = block.to_color();
                        color.r = (color.r as f32 * color_multiplier) as u8;
                        color.g = (color.g as f32 * color_multiplier) as u8;
                        color.b = (color.b as f32 * color_multiplier) as u8;

                        tile.texture[index] = [color.r, color.g, color.b];
                        height_map[index] = position[1] as i64;
                    }
                }
            }
        }
        tile
    }

    fn convert_dimension(x: usize, y: usize) -> usize {
        y * TEXTURE_SIZE as usize + x
    }

    fn get_relative_height(height: i64) -> f32 {
        let h = (height - TERRAIN_START).max(0);
        let h = h as f32 / (TERRAIN_END - TERRAIN_START) as f32;

        h.min(1.0)
    }
}
