use std::time::Duration;

// use tokio::sync::mpsc::{error::*, *};
use async_std::{channel::*, future};

pub fn async_channel<T>(bound: Option<usize>) -> (AsyncSender<T>, AsyncReceiver<T>) {
    // compiler gets a bit confused of generics
    let (sender, receiver): (Sender<T>, Receiver<T>) = if let Some(bound) = bound {
        bounded(bound)
    } else {
        unbounded()
    };
    (AsyncSender::<T> { sender }, AsyncReceiver::<T> { receiver })
}

#[derive(Debug, Clone)]
pub struct AsyncSender<T> {
    sender: Sender<T>,
}
impl<T> AsyncSender<T> {
    pub async fn send(&self, t: T) -> Result<(), super::SendError<T>> {
        match self.sender.send(t).await {
            Ok(_) => Ok(()),
            Err(e) => Err(super::SendError(e.0)),
        }
    }
}

#[derive(Debug)]
pub struct AsyncReceiver<T> {
    receiver: Receiver<T>,
}
impl<T> AsyncReceiver<T> {
    pub async fn recv(&mut self) -> Result<T, super::RecvError> {
        match self.receiver.recv().await {
            Ok(t) => Ok(t),
            Err(e) => Err(e.into()),
        }
    }

    pub async fn recv_timeout(&mut self, timeout: Duration) -> Result<T, super::TryRecvError> {
        let Ok(recv) = future::timeout(
            timeout,
            async { self.recv().await }
        ).await else {
            return Err(super::TryRecvError::Empty)
        };

        match recv {
            Ok(t) => Ok(t),
            Err(_) => Err(super::TryRecvError::Closed),
        }
    }

    pub fn try_recv(&mut self) -> Result<T, super::TryRecvError> {
        match self.receiver.try_recv() {
            Ok(t) => Ok(t),
            Err(e) => Err(e.into()),
        }
    }
}
