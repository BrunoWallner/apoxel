use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ClientError {
    Register,
    Login(LoginError),
    Logoff,
    ConnectionReset,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum LoginError {
    AlreadyLoggedIn,
    InvalidToken,
}
