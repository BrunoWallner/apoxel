use crate::color::Color;
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Deserialize, Serialize, PartialEq, PartialOrd)]
pub enum Block {
    Color(Color),
    Light(f32, f32, f32),
    None,
}

impl Block {
    pub fn to_color(&self) -> Color {
        match self {
            Self::Color(c) => *c,
            Self::Light(..) => Color::white(),
            _ => Color::black(),
        }
    }
}
