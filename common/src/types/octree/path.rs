use super::Side;
use serde::{Deserialize, Serialize};

// corner arrangement order
// **bottom**
// 0 1
// 2 3
// ---
// **top**
// 4 5
// 6 7

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
#[repr(u8)]
pub enum Corner {
    // LEFT, BOTTOM, BACK
    LBB = 0,
    RBB = 1,
    LBF = 2,
    RBF = 3,

    LTB = 4,
    RTB = 5,
    LTF = 6,
    RTF = 7,
}
impl Corner {
    // null-point is left bottom back
    pub fn from_array(array: [bool; 3]) -> Self {
        match array {
            [false, false, false] => Self::LBB,
            [true, false, false] => Self::RBB,
            [false, false, true] => Self::LBF,
            [true, false, true] => Self::RBF,

            [false, true, false] => Self::LTB,
            [true, true, false] => Self::RTB,
            [false, true, true] => Self::LTF,
            [true, true, true] => Self::RTF,
        }
    }
    pub fn get_offset(&self, size: f32) -> [f32; 3] {
        use Corner::*;
        match self {
            LBB => [0.0, 0.0, 0.0],
            RBB => [size, 0.0, 0.0],
            LBF => [0.0, 0.0, size],
            RBF => [size, 0.0, size],
            LTB => [0.0, size, 0.0],
            RTB => [size, size, 0.0],
            LTF => [0.0, size, size],
            RTF => [size, size, size],
        }
    }
    pub const fn all() -> [Corner; 8] {
        use Corner::*;
        [LBB, RBB, LBF, RBF, LTB, RTB, LTF, RTF]
    }

    pub fn from_side(side_position: &Side) -> [Corner; 4] {
        use Corner::*;
        use Side::*;
        match side_position {
            Left => [LBB, LBF, LTB, LTF],
            Right => [RBB, RBF, RTB, RTF],
            Back => [LBB, RBB, LTB, RTB],
            Front => [LBF, RBF, LTF, RTF],
            Top => [LTB, RTB, LTF, RTF],
            Bottom => [LBB, RBB, LBF, RBF],
        }
    }
}

pub fn get(size: u32, coord: &[u32; 3]) -> Vec<Corner> {
    let max_depth = (size as f32).log2() as usize; // needs unstable feature
    let mut path: Vec<Corner> = Vec::with_capacity(max_depth);

    let mut x_offset = 0;
    let mut y_offset = 0;
    let mut z_offset = 0;
    for depth in 1..=max_depth {
        let mut p: [bool; 3] = [false; 3];
        let dimension = size / 2_u32.pow(depth as u32);

        if coord[0] >= dimension + x_offset {
            x_offset += dimension;
            p[0] = true;
        }
        if coord[1] >= dimension + y_offset {
            y_offset += dimension;
            p[1] = true;
        }
        if coord[2] >= dimension + z_offset {
            z_offset += dimension;
            p[2] = true;
        }

        path.push(Corner::from_array(p));
    }

    path
}
