use super::node::Node;
use super::path;
use super::TraverseOutput;

use super::path::Corner;
use super::side::Neighbour;
use serde::{Deserialize, Serialize};
use std::marker::PhantomData;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Octree<T: Clone> {
    nodes: Node<T>,
}
impl<T: Clone + PartialEq> Octree<T> {
    pub fn default() -> Octree<T> {
        Octree { nodes: Node::Empty }
    }

    pub fn get_root_node(&self) -> &Node<T> {
        &self.nodes
    }

    pub fn get_root_node_mut(&mut self) -> &mut Node<T> {
        &mut self.nodes
    }

    pub fn traverse(&self, max_depth: Option<u32>) -> (u32, Vec<TraverseOutput<T>>) {
        let mut current_depth: u32 = 0;
        let output = self
            .nodes
            .traverse(&mut current_depth, &Corner::all(), max_depth);

        (current_depth, output)
    }

    pub fn traverse_only(
        &self,
        corners: &[Corner],
        max_depth: Option<u32>,
    ) -> (u32, Vec<TraverseOutput<T>>) {
        let mut current_depth: u32 = 0;
        let output = self.nodes.traverse(&mut current_depth, corners, max_depth);

        (current_depth, output)
    }

    pub fn merge(&mut self, other: &Self, max_depth: Option<u32>) {
        let (_depth, out) = other.traverse(max_depth);
        for mut out in out {
            let size = Octree::get_size_from_depth(out.depth);
            out.multiply_position(size as f32);
            let pos = out.position;
            self.set_node_xyz(size, pos[0] as u32, pos[1] as u32, pos[2] as u32, out.node);
        }
    }

    pub fn pack(&mut self) {
        self.nodes.pack();
    }

    pub fn set_xyz(&mut self, size: u32, x: u32, y: u32, z: u32, t: T) {
        let path = path::get(size, &[x, y, z]);
        self.nodes.set(&path, Node::Filled(t));
    }

    pub fn set_node_xyz(&mut self, size: u32, x: u32, y: u32, z: u32, node: Node<T>) {
        let path = path::get(size, &[x, y, z]);
        self.nodes.set(&path, node);
    }

    pub fn get_node_xyz(&mut self, size: u32, x: u32, y: u32, z: u32) -> (&Node<T>, u32) {
        let path = path::get(size, &[x, y, z]);
        let (node, layer) = self.nodes.get(&path);

        (node, layer)
    }

    pub fn is_empty(&self) -> bool {
        self.nodes == Node::Empty
    }

    pub fn get_neighbours(
        &self,
        neighbour: &Neighbour,
        size: u32,
        x: u32,
        y: u32,
        z: u32,
    ) -> Option<Vec<TraverseOutput<T>>> {
        let offset = neighbour.get_offset();

        let x = x as i32 + offset[0] as i32;
        let y = y as i32 + offset[1] as i32;
        let z = z as i32 + offset[2] as i32;

        // bounds check, if at bound set neighbour to true
        if x < 0 || x >= size as i32 || y < 0 || y >= size as i32 || z < 0 || z >= size as i32 {
            return None;
        }

        let path = path::get(size, &[x as u32, y as u32, z as u32]);
        let (node, mut current_depth) = self.nodes.get(&path);
        let max_depth = Some(current_depth.clone() + 1);
        return Some(node.traverse(&mut current_depth, &Corner::all(), max_depth));
    }

    pub fn merge_with_blacklist<F: Fn(T) -> bool>(
        &mut self,
        other: &Self,
        max_depth: Option<u32>,
        // blacklist: &HashSet<T>,
        blacklist: F,
    ) {
        let (_depth, out) = other.traverse(max_depth);
        for mut out in out {
            let is_blacklisted = match out.node.to_end().0 {
                Some(node) => blacklist(node),
                None => true,
            };
            if !is_blacklisted {
                let size = Octree::get_size_from_depth(out.depth);
                out.multiply_position(size as f32);
                let pos = out.position;
                self.set_node_xyz(size, pos[0] as u32, pos[1] as u32, pos[2] as u32, out.node);
            }
        }
    }

    pub fn sample_down(&mut self, depth: u32) {
        self.nodes.sample_down(depth, 0);
    }

    pub fn make_dense(&mut self, layer: u32) {
        self.nodes.make_dense(layer);
    }

    pub fn get_xyz(&self, size: u32, x: u32, y: u32, z: u32) -> (Option<T>, u32) {
        let path = path::get(size, &[x, y, z]);
        let (node, layer) = self.nodes.get(&path);
        let (node, layer_2) = node.to_end();

        (node, layer + layer_2)
    }
    
}

// impl<T: Clone + PartialEq + Hash + Ord> Octree<T> {
//     pub fn merge_with_blacklist<F: Fn(T) -> bool>(
//         &mut self,
//         other: &Self,
//         max_depth: Option<u32>,
//         // blacklist: &HashSet<T>,
//         blacklist: F,
//     ) {
//         let (_depth, out) = other.traverse(max_depth);
//         for mut out in out {
//             let is_blacklisted = match out.node.to_end().0 {
//                 Some(node) => blacklist(node),
//                 None => true,
//             };
//             if !is_blacklisted {
//                 let size = Octree::get_size_from_depth(out.depth);
//                 out.multiply_position(size as f32);
//                 let pos = out.position;
//                 self.set_node_xyz(size, pos[0] as u32, pos[1] as u32, pos[2] as u32, out.node);
//             }
//         }
//     }

//     pub fn sample_down(&mut self, depth: u32) {
//         self.nodes.sample_down(depth, 0);
//     }

//     pub fn make_dense(&mut self, layer: u32) {
//         self.nodes.make_dense(layer);
//     }

//     pub fn get_xyz(&self, size: u32, x: u32, y: u32, z: u32) -> (Option<T>, u32) {
//         let path = path::get(size, &[x, y, z]);
//         let (node, layer) = self.nodes.get(&path);
//         let (node, layer_2) = node.to_end();

//         (node, layer + layer_2)
//     }
// }

impl Octree<PhantomData<bool>> {
    #[inline(always)]
    pub fn get_depth_from_size(size: u32) -> u32 {
        (size as f32).log2() as u32
    }

    #[inline(always)]
    pub const fn get_size_from_depth(depth: u32) -> u32 {
        2_u32.pow(depth)
    }
}
