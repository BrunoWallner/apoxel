/// parses the tokens into the AST
/// after a ParseFunction returns None the pointer of the TokenStream will be reset
use crate::lexer::{DataType, TokenKind, TokenStream};

use super::combinator::any;
use super::{AstNode, AstNodeData, BinaryOperator, Data as AstData, FnArgument};

pub fn parse(mut input: TokenStream) -> Vec<AstNode> {
    let mut nodes = Vec::new();

    while !input.is_empty() {
        if let Some(node) = node(&mut input) {
            nodes.push(node);
        } else {
            input.advance(1);
        }
        input.skip_if(&TokenKind::Semicolon);
    }
    nodes
}

trait ParseFunction {
    fn name(&self) -> &'static str;
    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode>;
}

fn node_filter(input: &mut TokenStream, filter: &[&'static str]) -> Option<AstNode> {
    // order matters
    let mut fns: Vec<&dyn ParseFunction> = vec![
        &Binary {},
        &FnDeclaration {},
        &FnCall {},
        &Block {},
        &Wrap {},
        &VarDeclaration {},
        &Return {},
        &Data {},
        &Identifier {},
    ];
    fns.retain(|func| !filter.contains(&func.name()));
    let functions: Vec<_> = fns.iter().map(|f| f.func()).collect();

    let node = any(&functions, input);
    node
}

fn node(input: &mut TokenStream) -> Option<AstNode> {
    node_filter(input, &[])
}

struct Binary {}
impl Binary {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let mut tmp_input = input.clone();
        let left = Box::new(node_filter(&mut tmp_input, &["binary"])?);
        // let index = input[0].index;
        let index = input.peek(0)?.index;
        

        let op = tmp_input.next()?;
        let operator = BinaryOperator::from_tokenkind(&op.kind)?;

        let right = Box::new(node(&mut tmp_input)?);

        *input = tmp_input;

        Some(AstNode::new(
            AstNodeData::BinaryOperation {
                operator,
                left,
                right,
            },
            index,
        ))
    }
}
impl ParseFunction for Binary {
    fn name(&self) -> &'static str {
        "binary"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct Block {}
impl Block {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let index = input.peek(0)?.index;
        input.skip_if(&TokenKind::LeftBrace)?;

        let inner =
            input.peek_pair_counting_stripped(&TokenKind::LeftBrace, &TokenKind::RightBrace)?;
        let inner_len = inner.len();
        let mut nodes = Vec::new();
        let mut inner = TokenStream::new(inner);
        while let Some(node) = node(&mut inner) {
            nodes.push(node);
            if inner.skip_if(&TokenKind::Semicolon).is_none() {
                break;
            }
        }
        // only if successfull
        // +1 because of omitted right brace
        input.advance(inner_len + 1);

        Some(AstNode::new(AstNodeData::Block { block: nodes }, index))
    }
}
impl ParseFunction for Block {
    fn name(&self) -> &'static str {
        "block"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct Wrap {}
impl Wrap {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let index = input.peek(0)?.index;
        input.skip_if(&TokenKind::LeftParen)?;

        let inner =
            input.peek_pair_counting_stripped(&TokenKind::LeftParen, &TokenKind::RightParen)?;
        let inner_len = inner.len();
        let mut stream = TokenStream::new(inner);
        let node = Box::new(node(&mut stream)?);

        // only if successfull
        // +1 because of omitted right brace
        input.advance(inner_len + 1);

        Some(AstNode::new(AstNodeData::Wrap { wrap: node }, index))
    }
}
impl ParseFunction for Wrap {
    fn name(&self) -> &'static str {
        "wrap"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct FnDeclaration {}
impl FnDeclaration {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let index = input.peek(0)?.index;
        input.skip_if(&TokenKind::Fn)?;

        let peek = input.peek(0)?.clone();
        (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
        let DataType::String(name) = peek.data? else {
            return None;
        };
        let arguments = Self::parse_fn_arguments(input)?;
        let returns = Self::parse_fn_return(input);

        let body = Box::new(node(input)?);

        return Some(AstNode::new(
            AstNodeData::FnDeclaration {
                name,
                arguments,
                returns,
                body,
            },
            index,
        ));
    }
    fn parse_fn_return(input: &mut TokenStream) -> Option<String> {
        input.skip_if(&TokenKind::Arrow)?;

        let peek = input.peek(0)?.clone();
        (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
        match peek.data? {
            DataType::String(s) => Some(s),
            _ => None,
        }
    }

    fn parse_fn_arguments(input: &mut TokenStream) -> Option<Vec<FnArgument>> {
        input.skip_if(&TokenKind::LeftParen)?;
        let mut arguments = Vec::new();
        while let Some(argument) = parse_fn_inner(input) {
            arguments.push(argument)
        }
        (input.peek(0)?.kind == TokenKind::RightParen).then(|| input.advance(1))?;

        return Some(arguments);

        fn parse_fn_inner(input: &mut TokenStream) -> Option<FnArgument> {
            let peek = input.peek(0)?.clone();
            (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
            let name = match peek.data? {
                DataType::String(s) => Some(s),
                _ => None,
            }?;

            // (input.peek(0)?.kind == TokenKind::Colon).then(|| input.advance(1))?;
            input.skip_if(&TokenKind::Colon)?;

            let peek = input.peek(0)?.clone();
            (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
            let data_type = match peek.data? {
                DataType::String(s) => Some(s),
                _ => None,
            }?;

            // no ? at end, does not need to occur, but if it occurs do not fail but skip
            (input.peek(0)?.kind == TokenKind::Comma).then(|| input.advance(1));

            Some(FnArgument { name, data_type })
        }
    }
}
impl ParseFunction for FnDeclaration {
    fn name(&self) -> &'static str {
        "fn_declaration"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct FnCall {}
impl FnCall {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let peek = input.peek(0)?.clone();
        let index = peek.index;

        (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
        let DataType::String(name) = peek.data? else {
            return None;
        };
        // let index = input.peek(0)?.index;

        // (input.peek(0)?.kind == TokenKind::LeftParen).then(|| input.advance(1))?;
        input.skip_if(&TokenKind::LeftParen)?;
        let inner =
            input.peek_pair_counting_stripped(&TokenKind::LeftParen, &TokenKind::RightParen)?;
        let inner_len = inner.len();
        let mut arguments = Vec::new();
        let mut inner = TokenStream::new(inner);
        while let Some(node) = node(&mut inner) {
            arguments.push(node);
            if inner.skip_if(&TokenKind::Comma).is_none() {
                break;
            }
        }
        // +1 because of omitted right paren
        input.advance(inner_len + 1);

        Some(AstNode::new(AstNodeData::FnCall { name, arguments }, index))
    }
}
impl ParseFunction for FnCall {
    fn name(&self) -> &'static str {
        "fncall"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct Identifier {}
impl Identifier {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let peek = input.peek(0)?.clone();
        let index = peek.index;

        (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
        let Some(DataType::String(value)) = peek.data.clone() else {
            return None;
        };
        return Some(AstNode::new(AstNodeData::Identifier { value }, index));
    }
}
impl ParseFunction for Identifier {
    fn name(&self) -> &'static str {
        "identifier"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct Data {}
impl Data {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        any(&[Self::parse_literals, Self::parse_array], input)
    }

    fn parse_literals(input: &mut TokenStream) -> Option<AstNode> {
        let peek = input.peek(0)?;
        let index = peek.index;
        if peek.kind == TokenKind::StringLiteral || peek.kind == TokenKind::NumberLiteral {
            let data = peek.data.clone()?;
            input.advance(1);
            return Some(AstNode::new(
                AstNodeData::Data {
                    data: AstData::Base(data),
                },
                index,
            ));
        }
        return None;
    }

    fn parse_array(input: &mut TokenStream) -> Option<AstNode> {
        let index = input.peek(0)?.index;
        input.skip_if(&TokenKind::LeftBracket)?;

        let inner =
            input.peek_pair_counting_stripped(&TokenKind::LeftBracket, &TokenKind::RightBracket)?;
        let inner_len = inner.len();
        let mut array = Vec::new();
        let mut inner = TokenStream::new(inner);
        while let Some(node) = node(&mut inner) {
            array.push(node);
            // when true handle "[T; N]" case
            if inner.skip_if(&TokenKind::Comma).is_none() {
                if array.len() == 1 && inner.skip_if(&TokenKind::Semicolon).is_some() {
                    let peek = inner.peek(0)?.clone();
                    (peek.kind == TokenKind::NumberLiteral).then(|| inner.advance(1))?;
                    let Some(DataType::Num(value)) = peek.data.clone() else {
                        return None;
                    };
                    let count = (value as i64 - 1).max(0);
                    for _ in 0..count {
                        array.push(array[0].clone());
                    }
                }
                break;
            }
        }
        // only if successfull
        // +1 because of omitted right bracket
        input.advance(inner_len + 1);

        Some(AstNode::new(
            AstNodeData::Data {
                data: AstData::Array(array),
            },
            index,
        ))
    }
}
impl ParseFunction for Data {
    fn name(&self) -> &'static str {
        "data"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct Return {}
impl Return {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let index = input.peek(0)?.index;
        let peek = input.peek(0)?;
        (peek.kind == TokenKind::Return).then(|| input.advance(1))?;
        let value = if let Some(value) = node(input) {
            Some(Box::new(value))
        } else {
            None
        };
        return Some(AstNode::new(AstNodeData::Return { value }, index));
    }
}
impl ParseFunction for Return {
    fn name(&self) -> &'static str {
        "return"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}

struct VarDeclaration {}
impl VarDeclaration {
    fn parse(input: &mut TokenStream) -> Option<AstNode> {
        let index = input.peek(0)?.index;
        input.skip_if(&TokenKind::Let)?;
        let peek = input.peek(0)?.clone();
        (peek.kind == TokenKind::Identifier).then(|| input.advance(1))?;
        // if peek.kind == TokenKind::Identifier {
        let Some(DataType::String(name)) = peek.data else {
            return None;
        };

        // (input.peek(0)?.kind == TokenKind::Equal).then(|| input.advance(1))?;
        input.skip_if(&TokenKind::Equal)?;

        let value = Box::new(node(input)?);
        Some(AstNode::new(
            AstNodeData::VarDeclaration { name, value },
            index,
        ))
    }
}
impl ParseFunction for VarDeclaration {
    fn name(&self) -> &'static str {
        "var_declaration"
    }

    fn func(&self) -> fn(&mut TokenStream) -> Option<AstNode> {
        Self::parse
    }
}
