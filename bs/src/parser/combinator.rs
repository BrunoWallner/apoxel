use crate::lexer::TokenStream;

pub fn any<O>(function: &[fn(&mut TokenStream) -> Option<O>], input: &mut TokenStream) -> Option<O> {
    for f in function.into_iter() {
        let pointer = input.pointer();
        if let Some(output) = f(input) {
            return Some(output)
        } else {
            input.set_pointer(pointer)
        }
    }
    None
}
// pub fn any<I, O>(function: &[&dyn Fn(&mut I) -> Option<O>], input: &mut I) -> Option<O> {
//     for f in function.into_iter() {
//         if let Some(output) = f(input) {
//             return Some(output)
//         }
//     }
//     None
// }

// pub fn take<I, O>(function: &dyn Fn(&mut I) -> Option<O>, input: &mut I, amount: usize) -> Option<Vec<O>> {
//     // let mut output = [MaybeUninit::uninit(); U];
//     let mut output = Vec::new();
//     for _ in 0..amount {
//         let Some(o) = function(input) else {return None};
//         output.push(o);
//     }
//     Some(output)
// }

// pub fn is<I, C, const U: usize>(input: [I; U]) -> bool {
    
// }
