use colored::*;

#[derive(Clone, Debug)]
pub struct Error {
    pub at: usize,
    pub cause: String,
}
impl Error {
    pub fn new(at: usize, cause: String) -> Self {
        Self { at, cause }
    }

    pub fn format_with(&self, input: &str) -> String {
        let (line, start_offset) = get_line_at_index(input, self.at);
        // let line = &input[self.at .. self.at + 10];
        let line_number = get_line_number(input, self.at);
        let header = format!(
            "\n{}\n  {}",
            "execution error:".red().bold(),
            "▍".blue().bold()
        );
        let body = format!(
            "{} {} {}",
            format!("{}", line_number).blue().bold(),
            "▍".blue().bold(),
            line
        );
        let footer = format!("  {} {}{}","▍".blue().bold(), " ".repeat(start_offset), "^^^".cyan(),);
        let footer = format!("{} - {}", footer, self.cause.red());

        format!("{}\n{}\n{}", header, body, footer)
    }
}

fn get_line_at_index(text: &str, index: usize) -> (String, usize) {
    let mut line_start = 0;
    let mut line_end = 0;

    // Find the start and end indices of the line containing the given index
    for (i, c) in text.chars().enumerate() {
        if c == '\n' {
            if i >= index {
                break;
            }
            line_start = i + 1;
        }
        line_end = i;
    }

    // Extract the line from the text
    let line = &text[line_start..=line_end];
    let start_offset = index - line_start;
    (line.to_string(), start_offset)
}

fn get_line_number(text: &str, index: usize) -> usize {
    text[0..index].chars().filter(|x| x == &'\n').count() + 1
}
