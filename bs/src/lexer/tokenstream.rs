use std::ops::Index;

use super::{Token, TokenKind};
// use nom::{InputLength, InputTake};

#[derive(Clone, Debug)]
pub struct TokenStream {
    token: Vec<Token>,
    pointer: usize,
}

// impl InputLength for TokenStream {
//     fn input_len(&self) -> usize {
//         self.token.len()
//     }
// }

// impl InputTake for TokenStream {
//     fn take(&self, count: usize) -> Self {
//         TokenStream::new(&self.token[0..count])
//     }

//     fn take_split(&self, count: usize) -> (Self, Self) {
//         let (prefix, suffix) = self.token.split_at(count);
//         (TokenStream::new(suffix), TokenStream::new(prefix))
//     }
// }

impl TokenStream {
    pub fn new(token: &[Token]) -> Self {
        Self {
            token: Vec::from(token),
            pointer: 0,
        }
    }

    pub fn from(token: Vec<Token>) -> Self {
        Self {
            token,
            pointer: 0,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.token.is_empty() || self.token.len() <= self.pointer
    }

    pub fn pointer(&self) -> usize {
        self.pointer
    }

    pub fn set_pointer(&mut self, pointer: usize) {
        self.pointer = pointer
    }

    pub fn peek(&self, index: usize) -> Option<&Token> {
        self.token.get(self.pointer + index)
    }

    pub fn next(&mut self) -> Option<&Token> {
        let tmp = self.token.get(self.pointer);
        self.pointer += 1;
        tmp
    }

    pub fn skip_if(&mut self, token: &TokenKind) -> Option<()> {
        (&self.peek(0)?.kind == token).then(|| self.advance(1))
    }

    pub fn advance(&mut self, advance: usize) {
        self.pointer += advance;
    }

    /// the part that is checked against is not included
    pub fn peek_until(&mut self, token: &TokenKind) -> Option<&[Token]> {
        let mut index = 0;
        let mut end = None;
        while let Some(t) = self.peek(index) {
            if &t.kind == token {
                end = Some(index);
                break;
            }
            index += 1;
        }
        match end {
            Some(end) => Some(&self.token[self.pointer..self.pointer + end]),
            None => None,
        }
    }

    /// the part that is checked against is not included
    pub fn peek_until_last(&mut self, token: &TokenKind) -> Option<&[Token]> {
        let mut index = 0;
        let mut end = None;
        while let Some(t) = self.peek(index) {
            if &t.kind == token {
                end = Some(index);
                // break;
            }
            index += 1;
        }
        match end {
            Some(end) => Some(&self.token[self.pointer..self.pointer + end]),
            None => None,
        }
    }

    /// expects the first left token to be stripped
    /// ommits the last right token
    pub fn peek_pair_counting_stripped(
        &mut self,
        left: &TokenKind,
        right: &TokenKind,
    ) -> Option<&[Token]> {
        let mut index = 0;
        let mut end = None;
        let mut count = 1;
        while let Some(t) = self.peek(index) {
            if &t.kind == left {
                count += 1;
            }
            if &t.kind == right {
                end = Some(index);
                count -= 1;
                if count <= 0 {
                    break;
                }
            }
            index += 1;
        }
        match end {
            Some(end) => Some(&self.token[self.pointer..self.pointer + end]),
            None => None,
        }
    }

    /// expects the first left token not to be stripped
    /// ommits the last right token
    pub fn peek_pair_counting(&mut self, left: &TokenKind, right: &TokenKind) -> Option<&[Token]> {
        let mut index = 0;
        let mut end = None;
        let mut count = 0;
        while let Some(t) = self.peek(index) {
            if &t.kind == left {
                count += 1;
            }
            if &t.kind == right {
                end = Some(index);
                count -= 0;
                if count == 0 {
                    break;
                }
                // break;
            }
            index += 1;
        }
        match end {
            Some(end) => Some(&self.token[self.pointer..self.pointer + end]),
            None => None,
        }
    }

}

impl Index<usize> for TokenStream {
    type Output = Token;

    fn index(&self, index: usize) -> &Self::Output {
        &self.token[index]
    }
}

impl Iterator for TokenStream {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if self.token.len() > 0 {
            Some(self.token.remove(0))
        } else {
            None
        }
    }
}

impl FromIterator<Token> for TokenStream {
    fn from_iter<T: IntoIterator<Item = Token>>(iter: T) -> Self {
        let token: Vec<Token> = iter.into_iter().collect();
        TokenStream::from(token)
    }
}
