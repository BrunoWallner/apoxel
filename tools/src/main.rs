mod pregenerate;

use std::env::args;
use std::env::Args;
use std::num::ParseIntError;

const HELP: &str = "
USAGE:
    pregenerate xz-dimension y-dimension seed path
";

fn main() {
    let mut args = args();
    let _path = args.nth(0);
    if let Some(head) = args.nth(0) {
        if read_instruction(head.as_str(), args).is_err() {
            help();
        };
    } else {
        help();
    }
}

fn read_instruction(head: &str, mut args: Args) -> Result<(), ParseIntError> {
    match head {
        "pregenerate" => {
            if args.len() == 4 {
                let xz: i64 = args.nth(0).unwrap().parse::<i64>()?;
                let y: i64 = args.nth(0).unwrap().parse::<i64>()?;
                let seed: u32 = args.nth(0).unwrap().parse::<u32>()?;
                let path: String = args.nth(0).unwrap();

                println!("pregenerating...");
                pregenerate::pregenerate(xz, y, seed, path);
            } else {
                help();
            }
        }
        _ => help(),
    }

    Ok(())
}

fn help() {
    eprintln!("{}", HELP);
}
