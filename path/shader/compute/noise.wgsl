#define_import_path noise

// random
fn rng(seed: u32) -> f32 {
    // let seed = vars::vars.random_seed;
    let rng = pcg(seed);
    // vars::vars.random_seed += 1u;
    return f32(rng) / (pow(2.0, 32.0) - 1.0);
}

// https://www.pcg-random.org/
fn pcg(n: u32) -> u32 {
    var h = n * 747796405u + 2891336453u;
    h = ((h >> ((h >> 28u) + 4u)) ^ h) * 277803737u;
    return (h >> 22u) ^ h;
}

// 2D
fn mod289(x: vec2f) -> vec2f {
    return x - floor(x * (1. / 289.)) * 289.;
}

fn mod289_3(x: vec3f) -> vec3f {
    return x - floor(x * (1. / 289.)) * 289.;
}

fn permute3(x: vec3f) -> vec3f {
    return mod289_3(((x * 34.) + 1.) * x);
}

//  MIT License. © Ian McEwan, Stefan Gustavson, Munrocket
fn d2(v: vec2f) -> f32 {
    let C = vec4(
        0.211324865405187, // (3.0-sqrt(3.0))/6.0
        0.366025403784439, // 0.5*(sqrt(3.0)-1.0)
        -0.577350269189626, // -1.0 + 2.0 * C.x
        0.024390243902439 // 1.0 / 41.0
    );

    // First corner
    var i = floor(v + dot(v, C.yy));
    let x0 = v - i + dot(i, C.xx);

    // Other corners
    var i1 = select(vec2(0., 1.), vec2(1., 0.), x0.x > x0.y);

    // x0 = x0 - 0.0 + 0.0 * C.xx ;
    // x1 = x0 - i1 + 1.0 * C.xx ;
    // x2 = x0 - 1.0 + 2.0 * C.xx ;
    var x12 = x0.xyxy + C.xxzz;
    x12.x = x12.x - i1.x;
    x12.y = x12.y - i1.y;

    // Permutations
    i = mod289(i); // Avoid truncation effects in permutation

    var p = permute3(permute3(i.y + vec3(0., i1.y, 1.)) + i.x + vec3(0., i1.x, 1.));
    var m = max(0.5 - vec3(dot(x0, x0), dot(x12.xy, x12.xy), dot(x12.zw, x12.zw)), vec3(0.));
    m *= m;
    m *= m;

    // Gradients: 41 points uniformly over a line, mapped onto a diamond.
    // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
    let x = 2. * fract(p * C.www) - 1.;
    let h = abs(x) - 0.5;
    let ox = floor(x + 0.5);
    let a0 = x - ox;

    // Normalize gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt( a0*a0 + h*h );
    m *= 1.79284291400159 - 0.85373472095314 * (a0 * a0 + h * h);

    // Compute final noise value at P
    let g = vec3(a0.x * x0.x + h.x * x0.y, a0.yz * x12.xz + h.yz * x12.yw);
    return 130. * dot(m, g);
}


// 3D
fn permute4(x: vec4<f32>) -> vec4<f32> { return ((x * 34. + 1.) * x) % vec4<f32>(289.); }
fn taylorInvSqrt4(r: vec4<f32>) -> vec4<f32> { return 1.79284291400159 - 0.85373472095314 * r; }

fn d3(v: vec3<f32>) -> f32 {
  let C = vec2<f32>(1. / 6., 1. / 3.);
  let D = vec4<f32>(0., 0.5, 1., 2.);

  // First corner
  var i: vec3<f32>  = floor(v + dot(v, C.yyy));
  let x0 = v - i + dot(i, C.xxx);

  // Other corners
  let g = step(x0.yzx, x0.xyz);
  let l = 1.0 - g;
  let i1 = min(g.xyz, l.zxy);
  let i2 = max(g.xyz, l.zxy);

  // x0 = x0 - 0. + 0. * C
  let x1 = x0 - i1 + 1. * C.xxx;
  let x2 = x0 - i2 + 2. * C.xxx;
  let x3 = x0 - 1. + 3. * C.xxx;

  // Permutations
  i = i % vec3<f32>(289.);
  let p = permute4(permute4(permute4(
      i.z + vec4<f32>(0., i1.z, i2.z, 1. )) +
      i.y + vec4<f32>(0., i1.y, i2.y, 1. )) +
      i.x + vec4<f32>(0., i1.x, i2.x, 1. ));

  // Gradients (NxN points uniformly over a square, mapped onto an octahedron.)
  var n_: f32 = 1. / 7.; // N=7
  let ns = n_ * D.wyz - D.xzx;

  let j = p - 49. * floor(p * ns.z * ns.z); // mod(p, N*N)

  let x_ = floor(j * ns.z);
  let y_ = floor(j - 7.0 * x_); // mod(j, N)

  let x = x_ *ns.x + ns.yyyy;
  let y = y_ *ns.x + ns.yyyy;
  let h = 1.0 - abs(x) - abs(y);

  let b0 = vec4<f32>( x.xy, y.xy );
  let b1 = vec4<f32>( x.zw, y.zw );

  let s0 = floor(b0)*2.0 + 1.0;
  let s1 = floor(b1)*2.0 + 1.0;
  let sh = -step(h, vec4<f32>(0.));

  let a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  let a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  var p0: vec3<f32> = vec3<f32>(a0.xy, h.x);
  var p1: vec3<f32> = vec3<f32>(a0.zw, h.y);
  var p2: vec3<f32> = vec3<f32>(a1.xy, h.z);
  var p3: vec3<f32> = vec3<f32>(a1.zw, h.w);

  // Normalise gradients
  let norm = taylorInvSqrt4(vec4<f32>(dot(p0,p0), dot(p1,p1), dot(p2,p2), dot(p3,p3)));
  p0 = p0 * norm.x;
  p1 = p1 * norm.y;
  p2 = p2 * norm.z;
  p3 = p3 * norm.w;

  // Mix final noise value
  var m: vec4<f32> = 0.6 - vec4<f32>(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3));
  m = max(m, vec4<f32>(0.));
  m = m * m;
  return 42. * dot(m * m, vec4<f32>(dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3)));
}


// WORLEY
fn noise_permute_vec3f(x: vec3<f32>) -> vec3<f32> {
    return (((x * 34.0) + 10.0) * x) % 289.0;
}

fn d3_worley(v: vec3<f32>) -> vec2<f32> {
    let k = 0.142857142857;     // 1/7
    let ko = 0.428571428571;    // 1/2-k/2
    let k2 = 0.020408163265306; // 1/(7*7)
    let kz = 0.166666666667;    // 1/6
    let kzo = 0.416666666667;   // 1/2-1/6*2
    let jitter = 1.0;           // smaller jitter gives more regular pattern

    let pi = floor(v) % 289.0;
    let pf = fract(v) - 0.5;

    let pfx = pf.x + vec3(1.0, 0.0, -1.0);
    let pfy = pf.y + vec3(1.0, 0.0, -1.0);
    let pfz = pf.z + vec3(1.0, 0.0, -1.0);

    let p = noise_permute_vec3f(pi.x + vec3(-1.0, 0.0, 1.0));
    let p1 = noise_permute_vec3f(p + pi.y - 1.0);
    let p2 = noise_permute_vec3f(p + pi.y);
    let p3 = noise_permute_vec3f(p + pi.y + 1.0);

    let p11 = noise_permute_vec3f(p1 + pi.z - 1.0);
    let p12 = noise_permute_vec3f(p1 + pi.z);
    let p13 = noise_permute_vec3f(p1 + pi.z + 1.0);

    let p21 = noise_permute_vec3f(p2 + pi.z - 1.0);
    let p22 = noise_permute_vec3f(p2 + pi.z);
    let p23 = noise_permute_vec3f(p2 + pi.z + 1.0);

    let p31 = noise_permute_vec3f(p3 + pi.z - 1.0);
    let p32 = noise_permute_vec3f(p3 + pi.z);
    let p33 = noise_permute_vec3f(p3 + pi.z + 1.0);

    let ox11 = fract(p11 * k) - ko;
    let oy11 = (floor(p11 * k) % 7.0) * k - ko;
    let oz11 = floor(p11 * k2) * kz - kzo;  // p11 < 289 guaranteed

    let ox12 = fract(p12 * k) - ko;
    let oy12 = (floor(p12 * k) % 7.0) * k - ko;
    let oz12 = floor(p12 * k2) * kz - kzo;

    let ox13 = fract(p13 * k) - ko;
    let oy13 = (floor(p13 * k) % 7.0) * k - ko;
    let oz13 = floor(p13 * k2) * kz - kzo;

    let ox21 = fract(p21 * k) - ko;
    let oy21 = (floor(p21 * k) % 7.0) * k - ko;
    let oz21 = floor(p21 * k2) * kz - kzo;

    let ox22 = fract(p22 * k) - ko;
    let oy22 = (floor(p22 * k) % 7.0) * k - ko;
    let oz22 = floor(p22 * k2) * kz - kzo;

    let ox23 = fract(p23 * k) - ko;
    let oy23 = (floor(p23 * k) % 7.0) * k - ko;
    let oz23 = floor(p23 * k2) * kz - kzo;

    let ox31 = fract(p31 * k) - ko;
    let oy31 = (floor(p31 * k) % 7.0) * k - ko;
    let oz31 = floor(p31 * k2) * kz - kzo;

    let ox32 = fract(p32 * k) - ko;
    let oy32 = (floor(p32 * k) % 7.0) * k - ko;
    let oz32 = floor(p32 * k2) * kz - kzo;

    let ox33 = fract(p33 * k) - ko;
    let oy33 = (floor(p33 * k) % 7.0) * k - ko;
    let oz33 = floor(p33 * k2) * kz - kzo;

    let dx11 = pfx + jitter * ox11;
    let dy11 = pfy.x + jitter * oy11;
    let dz11 = pfz.x + jitter * oz11;

    let dx12 = pfx + jitter * ox12;
    let dy12 = pfy.x + jitter * oy12;
    let dz12 = pfz.y + jitter * oz12;

    let dx13 = pfx + jitter * ox13;
    let dy13 = pfy.x + jitter * oy13;
    let dz13 = pfz.z + jitter * oz13;

    let dx21 = pfx + jitter * ox21;
    let dy21 = pfy.y + jitter * oy21;
    let dz21 = pfz.x + jitter * oz21;

    let dx22 = pfx + jitter * ox22;
    let dy22 = pfy.y + jitter * oy22;
    let dz22 = pfz.y + jitter * oz22;

    let dx23 = pfx + jitter * ox23;
    let dy23 = pfy.y + jitter * oy23;
    let dz23 = pfz.z + jitter * oz23;

    let dx31 = pfx + jitter * ox31;
    let dy31 = pfy.z + jitter * oy31;
    let dz31 = pfz.x + jitter * oz31;

    let dx32 = pfx + jitter * ox32;
    let dy32 = pfy.z + jitter * oy32;
    let dz32 = pfz.y + jitter * oz32;

    let dx33 = pfx + jitter * ox33;
    let dy33 = pfy.z + jitter * oy33;
    let dz33 = pfz.z + jitter * oz33;

    var d11 = dx11 * dx11 + dy11 * dy11 + dz11 * dz11;
    var d12 = dx12 * dx12 + dy12 * dy12 + dz12 * dz12;
    var d13 = dx13 * dx13 + dy13 * dy13 + dz13 * dz13;
    var d21 = dx21 * dx21 + dy21 * dy21 + dz21 * dz21;
    var d22 = dx22 * dx22 + dy22 * dy22 + dz22 * dz22;
    var d23 = dx23 * dx23 + dy23 * dy23 + dz23 * dz23;
    var d31 = dx31 * dx31 + dy31 * dy31 + dz31 * dz31;
    var d32 = dx32 * dx32 + dy32 * dy32 + dz32 * dz32;
    var d33 = dx33 * dx33 + dy33 * dy33 + dz33 * dz33;

    let d1a = min(d11, d12);
    d12 = max(d11, d12);
    d11 = min(d1a, d13);        // Smallest now not in d12 or d13
    d13 = max(d1a, d13);
    d12 = min(d12, d13);        // 2nd smallest now not in d13
    let d2a = min(d21, d22);
    d22 = max(d21, d22);
    d21 = min(d2a, d23);        // Smallest now not in d22 or d23
    d23 = max(d2a, d23);
    d22 = min(d22, d23);        // 2nd smallest now not in d23
    let d3a = min(d31, d32);
    d32 = max(d31, d32);
    d31 = min(d3a, d33);        // Smallest now not in d32 or d33
    d33 = max(d3a, d33);
    d32 = min(d32, d33);        // 2nd smallest now not in d33
    let da = min(d11, d21);
    d21 = max(d11, d21);
    d11 = min(da, d31);         // Smallest now in d11
    d31 = max(da, d31);         // 2nd smallest now not in d31
    if d11.x > d11.y {
        let tmp = d11.x;
        d11.x = d11.y;
        d11.y = tmp;
    }
    if d11.x > d11.z {          // d11.x now smallest

        let tmp = d11.x;
        d11.x = d11.z;
        d11.z = tmp;
    }
    d12 = min(d12, d21);        // 2nd smallest now not in d21
    d12 = min(d12, d22);        // nor in d22
    d12 = min(d12, d31);        // nor in d31
    d12 = min(d12, d32);        // nor in d32
    d11.y = min(d11.y, d12.x);
    d11.z = min(d11.z, d12.y);  // nor in d12.yz
    d11.y = min(d11.y, d12.z);  // Only two more to go
    d11.y = min(d11.y, d11.z);  // Done! (phew!)
    return sqrt(d11.xy);        // F1, F2
}
