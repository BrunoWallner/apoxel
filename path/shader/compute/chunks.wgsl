#define_import_path chunks
#import aabb
#import ray
#import noise

const MAX_NODES: u32 = 50000000u;
const NODE_SIZE: u32 = 10u;
const U32MAX: u32 = 4294967295u;
const I32MAX: i32 = 2147483647;

@group(0)
@binding(0)
var<storage, read_write> data: array<u32>;

fn load(index: u32) -> u32 {
    return data[index];
}

fn store(index: u32, value: u32) {
    data[index] = value;
}

@group(0)
@binding(1)
var<storage, read> vars: Vars;
struct Vars {
    chunk_count: u32,
    data_size: u32,
    update_size: u32,
}


fn hit(ray: ray::Ray, origin: vec3<f32>, lod_distance: f32) -> ray::ChunkHit {
    var hit = ray::new_chunkhit();

    var mem_offset = 0u;
    // let chunk_length = data[mem_offset];
    let chunk_length = load(mem_offset);
    if chunk_length > 4u {
        // mem_offset += 1u; // offset into coords
        let node_position = vec3<i32>(
            i32(data[mem_offset + 1u]),
            i32(data[mem_offset + 2u]),
            i32(data[mem_offset + 3u]),
        );
        let a = aabb::Aabb(vec3<f32>(node_position), vec3<f32>(node_position) + vec3<f32>(1.0));
        let h = aabb::hit(a, ray);
        if h.hit || aabb::in_bound(a, ray.origin) {
            // add 4u to to mem offset to skip from coords and length to chunk_data
            return hit_node(mem_offset + 4u, ray, vec3<f32>(node_position), origin, lod_distance);
        }
    }

    return hit;
}

fn point(point: vec3<f32>, lod: u32) -> ray::ChunkHit {
    var hit = ray::new_chunkhit();

    var mem_offset = 0u;
    // let chunk_length = data[mem_offset];
    let chunk_length = load(mem_offset);
    if chunk_length > 4u {
        // mem_offset += 1u; // offset into coords
        let node_position = vec3<i32>(
            i32(data[mem_offset + 1u]),
            i32(data[mem_offset + 2u]),
            i32(data[mem_offset + 3u]),
        );
        let a = aabb::Aabb(vec3<f32>(node_position), vec3<f32>(node_position) + vec3<f32>(1.0));
        if aabb::in_bound(a, point) {
            // add 4u to to mem offset to skip from coords and length to chunk_data
            return point_node(mem_offset + 4u, point, vec3<f32>(node_position), lod);
        }
    }

    return hit;
}


struct PointContext {
    mem_offset: u32,
    node_position: vec3<f32>,
    point: vec3<f32>,
    node_size: f32,
    depth: i32,
    intersects: bool
};

fn point_node(mem_offset: u32, point: vec3<f32>, node_position: vec3<f32>, lod: u32) -> ray::ChunkHit {
    var hit = ray::new_chunkhit();
    var context = PointContext(
        mem_offset,
        node_position,
        point - node_position,
        1.0,
        0,
        true,
    );

    for (var iteration = 0u; iteration < 1000u; iteration += 1u) {
        // let node_type = data[context.mem_offset] & 1u;
        let node_type = load(context.mem_offset) & 1u;
        if !context.intersects {
            return hit;
        }

        if node_type == 1u {
            // group
            if context.depth > i32(lod) {
                context = point_to_end(context);
                break;
            } else {
                context = point_push(context);
            }

        } else {
            // leaf
            break;
        }
        
    }

    let node_type = load(context.mem_offset) & 1u;
    if node_type == 0u {
        let c = load(context.mem_offset + 2u);
        let color = vec3<u32>((c >> 8u) & 0xFFu, (c >> 16u) & 0xFFu, (c >> 24u) & 0xFFu);

        let lr = load(context.mem_offset + 3u);
        let lg = load(context.mem_offset + 4u);
        let lb = load(context.mem_offset + 5u);
        // let light = vec3<u32>((l >> 8u) & 0xFFu, (l >> 16u) & 0xFFu, (l >> 24u) & 0xFFu);
        // let light = f32(l) / f32(U32MAX);
        let light = vec3<f32>(bitcast<f32>(lr), bitcast<f32>(lg), bitcast<f32>(lb));
        
        hit.hit = true;
        hit.light = light;
        hit.color = vec3<f32>(color) / 255.0;
        hit.mem_offset = context.mem_offset;
        hit.node_size = context.node_size;
    }

    return hit;
}

struct HitContext {
    ray_direction: vec3<f32>,
    mem_offset: u32,
    node_position: vec3<f32>,
    pointer: vec3<f32>,
    node_size: f32,
    depth: i32,
    distance: f32,
};

fn hit_node(mem_offset: u32, ray: ray::Ray, node_position: vec3<f32>, origin: vec3<f32>, lod_distance: f32) -> ray::ChunkHit {
    // init some values
    var distance: f32 = 0.0;
    var hit = ray::new_chunkhit();
    
    // determine where ray first hits chunk
    let root_aabb = aabb::Aabb(
        node_position,
        node_position + 1.0,
    );
    var first_hit = ray.origin;
    if !aabb::in_bound(root_aabb, ray.origin) {
        let h = aabb::hit(root_aabb, ray);
        if h.hit {
            // ray.origin += ray.direction * (hit.distance + 0.001);
            // first_hit += ray.direction * (h.distance + 0.00001);
            first_hit += ray.direction * h.distance;
            distance += h.distance;
        } else {
            return hit;
        }
    }
    let first_hit_relative = first_hit - node_position;
    var context = HitContext(
        ray.direction,
        mem_offset,
        vec3<f32>(0.0, 0.0, 0.0),
        first_hit_relative,
        1.0,
        0,
        distance,
    );

    let bound = aabb::Aabb (
        vec3<f32>(0.0 - 1e-6),
        vec3<f32>(1.0 + 1e-6),
    );

    for (var iteration = 0u; iteration < 1000u; iteration += 1u) {
    // loop {
        if !aabb::in_bound(bound, context.pointer) {
            break;
        }

        let node_header = load(context.mem_offset);
        let node_type = node_header & 1u;

        if node_type == 1u {
            // group
            // var lod_d = distance(node_position + context.node_position, origin) / (LOD_DISTANCE * 1.0);
            // let lod = clamp(f32(lod) - log2(lod_d), 1.0, f32(lod));
            // let max_depth = i32(round(lod));
            let max_depth = calculate_lod(node_position + context.node_position, origin, lod_distance);

            if context.depth > i32(max_depth) {
                context = go_to_end(context);
                break;
            } else {
                context = push(context);
            }

        } else {
            // leaf
            break;
        }
    }

    // hit
    // let node_type = load(context.mem_offset) & 1u;
    let node_header = load(context.mem_offset);
    let node_type = node_header & 1u;

    if node_type == 0u {
        let c = load(context.mem_offset + 2u);
        let color = vec3<u32>((c >> 8u) & 0xFFu, (c >> 16u) & 0xFFu, (c >> 24u) & 0xFFu);
        let lr = load(context.mem_offset + 3u);
        let lg = load(context.mem_offset + 4u);
        let lb = load(context.mem_offset + 5u);
        let light = vec3<f32>(bitcast<f32>(lr), bitcast<f32>(lg), bitcast<f32>(lb));

        hit.hit = true;

        hit.mem_offset = context.mem_offset;
        hit.node_header = node_header;
        hit.color = vec3<f32>(color) / 255.0;
        hit.light = light;

        hit.distance = context.distance - context.node_size * 0.001;
        hit.position = context.pointer + node_position;
        hit.node_position = node_position + context.node_position + context.node_size * 0.5;

        let relative = (context.node_position + context.node_size / 2.0) - context.pointer;
        let normal = -aa_normal(relative);
        hit.normal = normal;
        hit.node_size = context.node_size;
        hit.position += hit.normal * (context.node_size * 0.01);

    }

    return hit;
}

fn calculate_lod(position: vec3<f32>, origin: vec3<f32>, distance: f32) -> u32 {
    var d = distance(position, origin);
    let lod = (1.0 / pow(d, 0.2)) * distance;
    let max_depth = u32(round(lod));
    return max_depth;
}

// go up
fn pop(c: HitContext) -> HitContext {
    var context = c;
    if context.depth > 0 {
        context.depth -= 1;
        let child_index = (load(context.mem_offset) >> 1u) & 7u;

        // go to parent
        context.mem_offset = load(context.mem_offset + 1u);

        let offset = offset_from_corner(child_index);
        context.node_position -= offset * context.node_size;

        context.node_size *= 2.0;
    }

    return context;
}

// go down
fn push(c: HitContext) -> HitContext {
    var context = c;
    let relative = context.pointer - context.node_position;
    let child_index: u32 = corner_from_offset(relative, context.node_size * 0.5);
    
    let child_mask: u32 = (load(context.mem_offset) >> 4u) & 0xFFu;
    let child_mask_bit: u32 = (child_mask >> child_index) & 1u;
    if child_mask_bit == 1u {
        // if child exists, just push into it and done
        context.node_size /= 2.0;
        context.node_position += offset_from_corner(child_index) * context.node_size;
        context.depth += 1;
        context.mem_offset = load(context.mem_offset + 2u + child_index);
    } else {
        // child does not exist, ray must advance further
        let node_size = context.node_size / 2.0;
        let node_position = context.node_position + offset_from_corner(child_index) * node_size;
        let depth = context.depth + 1;

        context = advance(context, node_size, node_position, depth);

        // pop into parent
        while context.depth > 0 {
            let aabb = aabb::Aabb(
                context.node_position,
                context.node_position + context.node_size,
            );
            if aabb::in_bound(aabb, context.pointer) {
                break;
            }
            context = pop(context);
        }
    }

    return context;
}

// advance ray
fn advance(c: HitContext, node_size: f32, node_position: vec3<f32>, depth: i32) -> HitContext {
    var context = c;
    let node_aabb = aabb::Aabb(
        node_position,
        node_position + node_size,
    );

    let cell_min = node_position;
    let cell_size = vec3<f32>(node_size); // 1.0?

    let side_pos = cell_min + step(vec3<f32>(0.0), context.ray_direction) * cell_size;
    let inv_dir = 1.0 / context.ray_direction;
    let side_dist = (side_pos - context.pointer) * inv_dir;

    let tmax = min(min(side_dist.x, side_dist.y), side_dist.z);
    if tmax >= 0.0 {
        let neighbour_min: vec3<f32> = select(
            cell_min,
            cell_min + sign(context.ray_direction) * node_size,
            vec3<f32>(tmax) == side_dist
        ) + 1e-5;
        let neighbour_max = neighbour_min + (node_size - 4e-5);

        context.pointer = clamp(context.pointer + context.ray_direction * tmax, neighbour_min, neighbour_max);
        context.distance += tmax;
    }

    return context;
}

fn go_to_end(c: HitContext) -> HitContext {
    var context = c;

     loop {
        let node_type = load(context.mem_offset) & 1u;
        if node_type == 0u {
            break;
        }

        let child_mask: u32 = (load(context.mem_offset) >> 4u) & 0xFFu;

        for (var o = 0u; o < 8u; o += 1u) {
            let child_mask_bit: u32 = (child_mask >> o) & 1u; 
            if child_mask_bit == 1u {
                context.mem_offset = load(context.mem_offset + 2u + o);
                // return context;
                break;
            }
            // if group is empty
            if o == 7u {
                // break;
                return context;
            }
        }
    }

    return context;
}

fn point_to_end(c: PointContext) -> PointContext {
    var context = c;

     loop {
        let node_type = load(context.mem_offset) & 1u;
        if node_type == 0u {
            break;
        }

        let child_mask: u32 = (load(context.mem_offset) >> 4u) & 0xFFu;

        for (var o = 0u; o < 8u; o += 1u) {
            let child_mask_bit: u32 = (child_mask >> o) & 1u; 
            if child_mask_bit == 1u {
                context.mem_offset = load(context.mem_offset + 2u + o);
                // return context;
                break;
            }
            // if group is empty
            if o == 7u {
                // break;
                return context;
            }
        }
    }

    return context;
}


// go down
fn point_push(c: PointContext) -> PointContext {
    var context = c;
    let relative = context.point - context.node_position;
    let child_index: u32 = corner_from_offset(relative, context.node_size * 0.5);

    let child_mask: u32 = (load(context.mem_offset) >> 4u) & 0xFFu;
    let child_mask_bit: u32 = (child_mask >> child_index) & 1u;
    if child_mask_bit == 1u {
        // if child exists, just push into it and done
        context.node_size /= 2.0;
        context.node_position += offset_from_corner(child_index) * context.node_size;
        context.depth += 1;

        context.mem_offset = load(context.mem_offset + 2u + child_index);
    } else {
        context.intersects = false;
    }

    return context;
}

// see `common/src/types/octree/path.rs`
fn corner_from_offset(position: vec3<f32>, threshold: f32) -> u32{
    var output = 0u;

    let pf = (sign(position - threshold) + 1.0) / 2.0;
    let p = vec3<u32>(pf);

    output |= (p.x);
    output |= (p.z << 1u);
    output |= (p.y << 2u);
    
    return output;
}

fn offset_from_corner(c: u32) -> vec3<f32>{
    var output = vec3<f32>(0.0);

    output.x = f32((c & 1u));       // if first bit is set then x = 1
    output.z = f32((c & 2u) >> 1u); // if second bit is set then z = 1
    output.y = f32((c & 4u) >> 2u); // if 3rd bit is set then y = 1

    return output;
}

fn aa_normal(hitPosition: vec3<f32>) -> vec3<f32>{
    let absHitPosition = abs(hitPosition);

    if absHitPosition.x > absHitPosition.y && absHitPosition.x > absHitPosition.z {
    // Hit the +X or -X face
        return vec3<f32>(sign(hitPosition.x), 0.0, 0.0);
    } else if absHitPosition.y > absHitPosition.x && absHitPosition.y > absHitPosition.z {
    // Hit the +Y or -Y face
        return vec3<f32>(0.0, sign(hitPosition.y), 0.0);
    } else {
    // Hit the +Z or -Z face
        return vec3<f32>(0.0, 0.0, sign(hitPosition.z));
    }
}
