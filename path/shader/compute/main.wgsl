const ANTIALIASING: f32 = 1.0;
const EPSILON: f32 = 0.00001;

@group(1)
@binding(1)
var<storage, read> camera: Camera;
struct Camera {
    position: vec3<f32>,
    yaw: f32,
    pitch: f32,
    fov: f32,
}

@group(1)
@binding(2)
var<storage, read_write> vars: Vars;
struct Vars {
    time: f32,
    random_seed: u32,
    aspect_ratio: f32,
    lod_distance: f32,
    shadows: u32,
    ao_resolution: u32,
    ao_strength: f32,
}

struct StorageTexture {
    width: u32,
    height: u32,
    data: array<vec4<u32>>,
}

@group(1)
@binding(3)
var<storage, read_write> position_storage: StorageTexture;

@group(1)
@binding(4)
var<storage, read_write> color_storage: StorageTexture;

@compute
@workgroup_size(16, 16)
fn main(
    @builtin(global_invocation_id) global_id: vec3<u32>,
) {

    let render_position = vec2<u32>(global_id.xy);
    // let output_size = vec2<u32>(textureDimensions(position_texture));
    let output_size = vec2<u32>(position_storage.width, position_storage.height);

    var rng = vars.random_seed;
    rng += rng * (render_position.x + (render_position.y * output_size.x));

    var rrp: vec2<f32> = (vec2<f32>(render_position) / vec2<f32>(output_size) - 0.5) * 2.0;

    rrp.x += noise::rng(rng) / f32(output_size.x) * ANTIALIASING;
    rrp.y += noise::rng(rng + 1u) / f32(output_size.y) * ANTIALIASING;

    let aspect_ratio = vars.aspect_ratio;

    if (render_position.x >= output_size.x || render_position.y >= output_size.y) {
        return;
    }   

    let yaw = mat3x3<f32>(
        vec3<f32>(cos(camera.yaw), 0.0, sin(camera.yaw)),
        vec3<f32>(0.0, 1.0, 0.0),
        vec3<f32>(-sin(camera.yaw), 0.0, cos(camera.yaw)),
    );
    let pitch = mat3x3<f32>(
        vec3<f32>(1.0, 0.0, 0.0),
        vec3<f32>(0.0, cos(camera.pitch), -sin(camera.pitch)),
        vec3<f32>(0.0, sin(camera.pitch), cos(camera.pitch)),
    );
    let rotation = yaw * pitch;

    let origin: vec3<f32> = camera.position;
    let ray_front = (rrp * vec2<f32>(aspect_ratio, 1.0));
    let direction = normalize(vec3<f32>(ray_front, pow(camera.fov, 4.0)));
    let ray = ray::Ray(origin, rotation * direction);

    // let sample = sample(ray, rng);
    let hit = chunks::hit(ray, camera.position, vars.lod_distance);

    let color_store = vec4<f32>(hit.color, 1.0);
    let position_store = vec4<f32>(hit.node_position, 1.0);

    // textureStore(color_texture, render_position, color_store);
    // textureStore(position_texture, render_position, color_store);

    let color_data = vec4<u32>(
        bitcast<u32>(color_store.r),
        bitcast<u32>(color_store.g),
        bitcast<u32>(color_store.b),
        bitcast<u32>(color_store.a),
    );
    let position_data = vec4<u32>(
        bitcast<u32>(position_store.r),
        bitcast<u32>(position_store.g),
        bitcast<u32>(position_store.b),
        bitcast<u32>(position_store.a),
    );
    let index = render_position.x + render_position.y * color_storage.width;
    position_storage.data[index] = position_data;
    color_storage.data[index] = color_data;
}

fn random_unit_vector(rng: u32) -> vec3<f32> {
    return normalize(vec3<f32>(noise::rng(rng + 0u) - 0.5, noise::rng(rng + 100u) - 0.5, noise::rng(rng + 2000u) - 0.5 ));
}

