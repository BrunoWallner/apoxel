use app::Application;
use common::prelude::{Chunk, Receiver};
use egui::Context;
use glam::Vec3;
use winit::{
    event::MouseButton,
    keyboard::KeyCode,
    window::{CursorGrabMode, Fullscreen},
};

mod app;
mod engine;
mod fractal;
mod log;
// mod gpu;
mod gui;
mod importer;
mod mc;
mod server;

fn main() {
    let app: app::Application = app::Application::new();
    app.run::<App>();
}

struct App {
    // input: Receiver<STC>,
    // output: Sender<CTS>,
    // _user: User,
    locked: bool,
    gui: gui::Gui,
    chunk_rx: Receiver<Chunk>,
}
impl app::ApplicationTrait for App {
    fn update(
        &mut self,
        dt: f32,
        engine: &mut engine::Engine,
        // input_manager: &mut app::InputManager,
        app: &mut Application,
    ) {
        let just_pressed = app.input_manager.get_just_pressed();
        let pressed = app.input_manager.get_pressed();
        let mouse_pressed = app.input_manager.get_pressed_mouse();
        let mouse_just_pressed = app.input_manager.get_just_pressed_mouse();
        let mouse = app.input_manager.get_mouse_delta();
        let mouse_wheel = app.input_manager.get_mouse_wheel_delta()[1];

        self.graphic_settings(&just_pressed, &pressed, engine, app);
        self.camera(&mouse, &mouse_wheel, engine);
        self.mouse(engine, &mouse_pressed, &mouse_just_pressed);

        engine.main_pass.vars.ao_resolution = self.gui.ao_resolution;
        engine.main_pass.vars.ao_strength = self.gui.ao_strength;
        engine.main_pass.vars.lod_distance = self.gui.lod_distance;
        // engine.stages.two.vars.max_lod = self.gui.max_lod;
        if self.gui.shadows {
            engine.main_pass.vars.shadows = 1;
        } else {
            engine.main_pass.vars.shadows = 0;
        }

        self.movement(dt, engine, &pressed);
        // engine.camera.position = self.position.into();

        while let Ok(chunk) = self.chunk_rx.try_recv() {
            // gpu.clear_chunks();
            let coord = [0, 0, 0];
            engine.chunks.push_chunk(chunk, coord);
        }

        // self.sync(engine);
    }

    fn setup(_engine: &mut engine::Engine) -> Self {
        let (chunk_tx, chunk_rx) = common::channel::channel(None);

        App {
            locked: false,
            gui: gui::Gui::new(chunk_tx),
            chunk_rx,
        }
    }

    fn exit(&self) {
    }
}

impl app::GuiTrait for App {
    fn render(&mut self, ctx: &Context) {
        self.gui.render(ctx);
    }
    fn update(&mut self, dt: f32) {
        self.gui.update(dt);
    }
}

impl App {
    fn graphic_settings(
        &mut self,
        just_pressed: &Vec<KeyCode>,
        pressed: &Vec<KeyCode>,
        engine: &mut engine::Engine,
        app: &mut Application,
    ) {
        for virtual_keycode in just_pressed {
            match virtual_keycode {
                KeyCode::NumpadAdd => {
                    if pressed.contains(&KeyCode::KeyT) {
                        app.time_multiplier += 1.0;
                    }
                }
                KeyCode::NumpadSubtract => {
                    if pressed.contains(&KeyCode::KeyT) {
                        app.time_multiplier -= 1.0;
                    }
                }
                KeyCode::Escape => {
                    self.locked = !self.locked;
                    if self.locked {
                        engine.core.window.set_cursor_visible(false);
                        if engine
                            .core
                            .window
                            .set_cursor_grab(CursorGrabMode::Locked)
                            .is_err()
                        {
                            let _ = engine.core.window.set_cursor_grab(CursorGrabMode::Confined);
                        }
                    } else {
                        engine.core.window.set_cursor_visible(true);
                        let _ = engine.core.window.set_cursor_grab(CursorGrabMode::None);
                    }
                }
                KeyCode::F11 => {
                    let fullscreen = match engine.core.window.fullscreen() {
                        Some(_) => None,
                        None => {
                            if pressed.contains(&KeyCode::ShiftLeft) {
                                let Some(monitor) = engine.core.window.current_monitor() else {
                                    return;
                                };
                                let Some(videomode) = monitor.video_modes().next() else {
                                    return;
                                };
                                Some(Fullscreen::Exclusive(videomode))
                            } else {
                                Some(Fullscreen::Borderless(None))
                            }
                        }
                    };
                    engine.core.window.set_fullscreen(fullscreen);
                }
                KeyCode::KeyR => {
                    engine.reload();
                }
                _ => (),
            }
        }
    }

    fn mouse(
        &self,
        engine: &mut engine::Engine,
        pressed: &[MouseButton],
        just_pressed: &[MouseButton],
    ) {
        if !self.locked {
            return;
        }
        // if just_pressed.contains(&MouseButton::Left) {
        //     engine.edit_pass.remove_block();
        // }
        // if pressed.contains(&MouseButton::Right) {
        //     engine.edit_pass.add_light();
        // }
    }

    fn movement(
        &self,
        dt: f32,
        // camera: &mut engine::CameraController,
        engine: &mut engine::Engine,
        pressed: &Vec<KeyCode>,
    ) {
        if !self.locked {
            return;
        }
        // movement
        let mut move_speed = dt * 0.000007;

        let forward = (engine.camera.camera.forward() * Vec3::new(1.0, 0.0, 1.0)).normalize();
        // let forward = camera.forward().normalize();
        let right = (engine.camera.camera.right() * Vec3::new(1.0, 0.0, 1.0)).normalize();
        let up = Vec3::new(0.0, 1.0, 0.0);

        if pressed.contains(&KeyCode::ControlLeft) {
            move_speed *= 10.0;
        }
        // let max_movement_forward = distances.forward / move_speed;

        let mut movement: Vec3 = Vec3::ZERO;
        for virtual_keycode in pressed.iter() {
            match virtual_keycode {
                KeyCode::KeyW => movement += forward,
                KeyCode::KeyA => movement -= right,
                KeyCode::KeyS => movement -= forward,
                KeyCode::KeyD => movement += right,
                KeyCode::Space => movement += up,
                KeyCode::ShiftLeft => movement -= up,
                _ => (),
            }
        }
        movement = movement.normalize_or_zero() * move_speed;

        engine.camera.movement(movement);
        // engine.camera.move_down(&engine.core, &engine.physics_pass);
    }

    fn camera(&self, mouse: &[f64; 2], mouse_wheel: &f64, engine: &mut engine::Engine) {
        let mouse_speed: f32 = 0.001;

        if self.locked {
            engine.camera.camera.yaw -= mouse[0] as f32 * mouse_speed;
            engine.camera.camera.pitch -= mouse[1] as f32 * mouse_speed;
            engine.camera.camera.fov += *mouse_wheel as f32 / 32.0;
        }
    }
}
