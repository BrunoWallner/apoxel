use common::blocks::Block;
use common::chunk::Chunk;
use common::color::Color;
use common::types::octree::{Corner, Node};
use noise::NoiseFn;
use rand::prelude::*;

pub fn import_fractal(size: u32) -> Chunk {
    log::info!("building fractal...");
    let mut rng = rand::thread_rng();
    let mut chunk = Chunk::default();
    // let size = 2_u32.pow(size);
    // for x in 0..size {
    //     for z in 0..size {
    //         for y in 0..size {
    //             let xf = (x as f64 / size as f64 - 0.5) * 5.0;
    //             let zf = (z as f64 / size as f64 - 0.5) * 5.0;
    //             let yf = (y as f64 / size as f64 - 0.5) * 5.0;
    //             if let Some(color) = spherical_fractal_color(xf, zf, yf, 200) {
    //                 // let a = 200;
    //                 // let color = Color::splat(a);
    //                 chunk.set_xyz(size, x, y, z, Block::Color(color));
    //             }
    //         }
    //     }
    // }
    let node = build_node(&mut rng, size, 0, 0);
    chunk.set_node_xyz(1, 0, 0, 0, node);

    chunk
}

fn build_node(rng: &mut ThreadRng, max_depth: u32, depth: u32, octant: u32) -> Node<Block> {
    let mut node = Node::Empty;
    // let leaf_

    if depth >= max_depth {
        return node;
    }

    let group = rng.gen_range(1..=7);
    let group2 = rng.gen_range(1..=7);
    let group3 = rng.gen_range(1..=7);
    let group4 = rng.gen_range(1..=7);
    let leaf = rng.gen_range(1..=7);
    if octant == 0 || octant == group || octant == group2 || octant == group3 || octant == group4 {
        let mut group = Node::get_empty_group();
        for octant in Corner::all() {
            group[octant as usize] = build_node(rng, max_depth, depth + 1, octant as u32);
        }
        node = Node::Group(group);
    } else if octant == leaf {
        let color = Color {
            r: octant as u8 * 32,
            g: 255 - octant as u8 * 32,
            b: (octant as u8) % 8 * 32,
        };
        node = Node::Filled(Block::Color(color));
    }
    node
}

pub fn import_noise(size: u32) -> Chunk {
    let noise = noise::Perlin::new(100);
    log::info!("building fractal...");
    let mut chunk = Chunk::default();
    let size = 2_u32.pow(size);
    for x in 0..size {
        for z in 0..size {
            let coord = [x as f64 / size as f64 * 5.0, z as f64 / size as f64 * 5.0];
            let mut y = (((noise.get(coord) + 1.0) * 0.5) * size as f64 * 0.2) as u32;
            let coord = [x as f64 / size as f64 * 15.0, z as f64 / size as f64 * 15.0];
            y += (((noise.get(coord) + 1.0) * 0.3) * size as f64 * 0.07) as u32;
            let color = Color {
                r: ((x * 255) / size) as u8,
                g: ((y * 255) / size) as u8,
                b: ((z * 255) / size) as u8,
            };
            let b = Block::Color(color);

            // for y in y..y + (size / 16).max(1) {
            //     // if y == size / 2 {
            //     chunk.set_xyz(size, x, y, z, b);
            // }

            chunk.set_xyz(size, x, y, z, b);
            chunk.set_xyz(size, x, y + 1, z, b);
        }
    }

    chunk
}

// pub fn import_noise(size: u32) -> Chunk {
//     log::info!("building fractal...");
//     let mut chunk = Chunk::default();
//     let size = 2_u32.pow(size);
//     let points = 100;
//     let radius = (size / 2) as f32;

//     for i in 0..points {
//         let u = i as f32 / (points - 1) as f32;
//         let phi = u * 2.0 * std::f32::consts::PI;
//         let cos_theta = 2.0 * u - 1.0;
//         let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

//         let x = phi.cos() * sin_theta * radius + radius / 2.0;
//         let y = phi.sin() * sin_theta * radius + radius / 2.0;
//         let z = cos_theta * radius + radius / 2.0;

//         let block = Block::Color(Color::splat(100));
//         chunk.set_xyz(size, x as u32, y as u32, z as u32, block);
//     }

//     chunk
// }
