use super::Gui;
use crate::{fractal, mc};
use egui::{CollapsingHeader, Context, Ui};
use std::{fs, thread};

pub struct McImport {
    pub offset_x: i32,
    pub offset_z: i32,
    pub y_range_bottom: i32,
    pub y_range_top: i32,
    pub scale: u32,
    pub size: u32,
}
impl Default for McImport {
    fn default() -> Self {
        Self {
            offset_x: 0,
            offset_z: 0,
            scale: 1,
            y_range_bottom: 32,
            y_range_top: 128,
            size: 1,
        }
    }
}

pub struct FractalImport {
    pub size: u32,
}
impl Default for FractalImport {
    fn default() -> Self {
        Self { size: 1 }
    }
}

#[derive(Default)]
pub struct Import {
    pub mc: McImport,
    pub fractal: FractalImport,
}

pub fn importer(gui: &mut Gui, ctx: &Context) {
    egui::Window::new("Load world")
        .resizable(true)
        .show(ctx, |ui| {
            CollapsingHeader::new("mc importer").show(ui, |mut ui| {
                mc(gui, &mut ui);
            });
            CollapsingHeader::new("fractal importer").show(ui, |mut ui| {
                fractal(gui, &mut ui);
            });
            CollapsingHeader::new("vox importer").show(ui, |mut ui| {
                vox(gui, &mut ui);
            });
        });
}

fn vox(gui: &mut Gui, ui: &mut Ui) {
    if ui.button("Open file").clicked() {
        if let Some(path) = rfd::FileDialog::new().pick_file() {
            gui.picked_path = Some(path.display().to_string());
        }
    }

    if let Some(picked_path) = &gui.picked_path {
        ui.horizontal(|ui| {
            ui.label("Picked folder:");
            // ui.label(picked_path);
            ui.add(egui::Label::new(picked_path).wrap())
        });
    }

    if ui.button("import").clicked() {
        let chunk_tx = gui.chunk_tx.clone();
        if let Some(path) = gui.picked_path.clone() {
            thread::spawn(move || {
                let bytes = fs::read(path).unwrap();
                chunk_tx
                    .send(common::voxel::parse(&bytes).unwrap())
                    .unwrap();
            });
        } else {
            log::warn!("no import folder selected");
        }
    }
}

fn mc(gui: &mut Gui, ui: &mut Ui) {
    if ui.button("Open folder").clicked() {
        if let Some(path) = rfd::FileDialog::new().pick_folder() {
            gui.picked_path = Some(path.display().to_string());
        }
    }

    if let Some(picked_path) = &gui.picked_path {
        ui.horizontal(|ui| {
            ui.label("Picked folder:");
            // ui.label(picked_path);
            ui.add(egui::Label::new(picked_path).wrap())
        });
    }

    egui::Grid::new("sliders").show(ui, |ui| {
        // ui.horizontal(|ui| {
        ui.label("offset x");
        ui.add(egui::Slider::new(&mut gui.import.mc.offset_x, -50..=50));
        ui.end_row();
        // });
        // ui.horizontal(|ui| {
        ui.label("offset_z");
        ui.add(egui::Slider::new(&mut gui.import.mc.offset_z, -50..=50));
        ui.end_row();
        // });
        // ui.horizontal(|ui| {
        ui.label("scale");
        ui.add(egui::Slider::new(&mut gui.import.mc.scale, 1..=16));
        ui.end_row();
        // });
        // ui.horizontal(|ui| {
        ui.label("y bottom");
        ui.add(egui::Slider::new(
            &mut gui.import.mc.y_range_bottom,
            -64..=255,
        ));
        ui.end_row();
        // });
        // ui.horizontal(|ui| {
        ui.label("y top");
        ui.add(egui::Slider::new(&mut gui.import.mc.y_range_top, -64..=255));
        ui.end_row();

        ui.label("size");
        ui.add(egui::Slider::new(&mut gui.import.mc.size, 1..=8));
        ui.end_row();

        // });
    });

    if ui.button("import").clicked() {
        let chunk_tx = gui.chunk_tx.clone();
        let x = gui.import.mc.offset_x;
        let z = gui.import.mc.offset_z;
        let s = gui.import.mc.scale as f32;
        let y_range = [gui.import.mc.y_range_bottom, gui.import.mc.y_range_top];
        let size = gui.import.mc.size;
        if let Some(path) = gui.picked_path.clone() {
            thread::spawn(move || {
                chunk_tx
                    .send(mc::import(&path, [x, z], s, y_range, size))
                    .unwrap();
            });
        } else {
            log::warn!("no import folder selected");
        }
    }
}

fn fractal(gui: &mut Gui, ui: &mut Ui) {
    egui::Grid::new("fractal sliders").show(ui, |ui| {
        ui.label("size");
        ui.add(egui::Slider::new(&mut gui.import.fractal.size, 1..=12));
        ui.end_row();

        // });
    });

    if ui.button("import fractal").clicked() {
        let chunk_tx = gui.chunk_tx.clone();
        let size = gui.import.fractal.size;
        thread::spawn(move || {
            chunk_tx.send(fractal::import_fractal(size)).unwrap();
        });
    }
    if ui.button("import noise").clicked() {
        let chunk_tx = gui.chunk_tx.clone();
        let size = gui.import.fractal.size;
        thread::spawn(move || {
            chunk_tx.send(fractal::import_noise(size)).unwrap();
        });
    }
}
