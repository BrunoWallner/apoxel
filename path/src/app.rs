use super::engine::Engine;
use egui::Context;
use std::time;
use std::{collections::HashSet, sync::Arc};
use winit::{
    event::*,
    event_loop::EventLoop,
    keyboard::{KeyCode, PhysicalKey},
    window::WindowBuilder,
};

pub trait ApplicationTrait {
    fn update(
        &mut self,
        dt: f32,
        engine: &mut Engine,
        app: &mut Application, // input_manager: &mut InputManager,
    );
    fn setup(engine: &mut Engine) -> Self;
    fn exit(&self);
}

pub trait GuiTrait {
    fn render(&mut self, ctx: &Context);
    fn update(&mut self, dt: f32);
}

pub struct InputManager {
    pressed_keys: HashSet<KeyCode>,
    just_pressed_keys: HashSet<KeyCode>,
    just_pressed_keys_block: HashSet<KeyCode>,
    pressed_mouse_buttons: HashSet<MouseButton>,
    just_pressed_mouse_buttons: HashSet<MouseButton>,
    just_pressed_mouse_buttons_block: HashSet<MouseButton>,
    mouse_delta: [f64; 2],
    mouse_wheel_delta: [f64; 2],
}
impl InputManager {
    pub fn new() -> Self {
        Self {
            pressed_keys: HashSet::new(),
            just_pressed_keys: HashSet::new(),
            just_pressed_keys_block: HashSet::new(),
            mouse_delta: [0.0; 2],
            mouse_wheel_delta: [0.0; 2],
            pressed_mouse_buttons: HashSet::default(),
            just_pressed_mouse_buttons: HashSet::default(),
            just_pressed_mouse_buttons_block: HashSet::default(),
        }
    }

    pub fn get_pressed(&self) -> Vec<KeyCode> {
        self.pressed_keys.iter().cloned().collect()
    }

    pub fn get_just_pressed(&mut self) -> Vec<KeyCode> {
        self.just_pressed_keys.drain().collect()
    }

    pub fn get_pressed_mouse(&self) -> Vec<MouseButton> {
        self.pressed_mouse_buttons.iter().cloned().collect()
    }

    pub fn get_just_pressed_mouse(&mut self) -> Vec<MouseButton> {
        self.just_pressed_mouse_buttons.drain().collect()
    }

    pub fn get_mouse_delta(&mut self) -> [f64; 2] {
        let delta = self.mouse_delta;
        self.mouse_delta = [0.0, 0.0];
        delta
    }

    pub fn get_mouse_wheel_delta(&mut self) -> [f64; 2] {
        let delta = self.mouse_wheel_delta;
        self.mouse_wheel_delta = [0.0, 0.0];
        delta
    }
}

pub struct Application {
    pub input_manager: InputManager,
    pub time_multiplier: f32,
}
impl Application {
    pub fn new() -> Self {
        let input_manager = InputManager::new();

        Self {
            input_manager,
            time_multiplier: 1.0,
        }
    }

    pub fn run<T: Sync + Send + ApplicationTrait + GuiTrait + 'static>(mut self) {
        // // must leak app for it to be 'static
        // let app = Box::new(app);
        // let mut app = Box::leak(app);

        let event_loop = EventLoop::new().unwrap();
        let window = WindowBuilder::new().build(&event_loop).unwrap();
        let window = Arc::new(window);
        let mut engine = async_std::task::block_on(Engine::new(window, [1920, 1080]));

        let mut app = <T as ApplicationTrait>::setup(&mut engine);

        let mut dt: f32 = 0.0;

        let mut timer = time::Instant::now();
        let _ = event_loop.run(move |event, ewlt| {
            match event {
                Event::WindowEvent {
                    ref event,
                    window_id,
                } if window_id == engine.core.window.id() => {
                    engine
                        .render
                        .egui
                        .handle_input(&mut engine.core.window, event);
                    match event {
                        WindowEvent::CloseRequested => {
                            app.exit();
                            // *control_flow = ControlFlow::Exit
                            ewlt.exit();
                        }
                        WindowEvent::Resized(physical_size) => {
                            engine.resize(*physical_size);
                        }
                        WindowEvent::KeyboardInput { event, .. } => match event {
                            KeyEvent {
                                state,
                                physical_key,
                                ..
                            } => {
                                let PhysicalKey::Code(keycode) = physical_key else {
                                    return;
                                };
                                match state {
                                    ElementState::Pressed => {
                                        self.input_manager.pressed_keys.insert(*keycode);
                                        if !self
                                            .input_manager
                                            .just_pressed_keys_block
                                            .contains(keycode)
                                        {
                                            self.input_manager.just_pressed_keys.insert(*keycode);
                                            self.input_manager
                                                .just_pressed_keys_block
                                                .insert(*keycode);
                                        }
                                    }
                                    ElementState::Released => {
                                        self.input_manager.pressed_keys.remove(keycode);
                                        self.input_manager.just_pressed_keys_block.remove(keycode);
                                    }
                                }
                            }
                        },
                        WindowEvent::MouseWheel { delta, .. } => match delta {
                            MouseScrollDelta::LineDelta(x, y) => {
                                self.input_manager.mouse_wheel_delta[0] += *x as f64;
                                self.input_manager.mouse_wheel_delta[1] += *y as f64;
                            }
                            MouseScrollDelta::PixelDelta(xy) => {
                                self.input_manager.mouse_wheel_delta[0] += xy.x;
                                self.input_manager.mouse_wheel_delta[1] += xy.y;
                            }
                        },
                        WindowEvent::RedrawRequested => {
                            GuiTrait::update(&mut app, dt);
                            engine.update();
                            engine.render(dt * self.time_multiplier, &mut app).unwrap();

                            let now = time::Instant::now();
                            dt = (now.duration_since(timer).as_nanos() as f64 / 1_000_000.0) as f32;
                            timer = now;
                            // let elapsed = timer.elapsed().as_nanos() as f64 / 1_000_000.0;
                            // dt = elapsed as f32;
                            ApplicationTrait::update(&mut app, dt, &mut engine, &mut self);
                            engine.core.window.request_redraw();
                        }

                        WindowEvent::MouseInput {
                            state,
                            button,
                            ..
                        } => match state {
                            ElementState::Pressed => {
                                self.input_manager.pressed_mouse_buttons.insert(*button);
                                if !self.input_manager.just_pressed_mouse_buttons_block.contains(button) {
                                    self.input_manager.just_pressed_mouse_buttons.insert(*button);
                                    self.input_manager.just_pressed_mouse_buttons_block.insert(*button);
                                }
                            }
                            ElementState::Released => {
                                self.input_manager.pressed_mouse_buttons.remove(button);
                                self.input_manager.just_pressed_mouse_buttons_block.remove(button);
                            }
                        },

                        _ => (),
                    }
                }
                // Event::MainEventsCleared => {
                //     // RedrawRequested will only trigger once, unless we manually
                //     // request it.
                //     let now = time::Instant::now();
                //     dt = (now.duration_since(timer).as_nanos() as f64 / 1_000_000.0) as f32;
                //     timer = now;
                //     // let elapsed = timer.elapsed().as_nanos() as f64 / 1_000_000.0;
                //     // dt = elapsed as f32;
                //     app.update(dt, &mut gpu, &mut control_flow, &mut self);
                //     gpu.window.request_redraw();
                // }
                Event::DeviceEvent { event, .. } => match event {
                    DeviceEvent::MouseMotion { delta } => {
                        self.input_manager.mouse_delta[0] += delta.0;
                        self.input_manager.mouse_delta[1] += delta.1;
                    }
                    _ => (),
                },

                _ => (),
            };
        });
    }
}
