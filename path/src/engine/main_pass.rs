use crate::importer;

use super::{
    camera::Camera,
    chunk::ChunkManager,
    core::Core,
    entry::{Entry, Group, Storage},
    Pass,
};

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vars {
    time: f32,
    random_seed: u32,
    aspect_ratio: f32,
    pub lod_distance: f32,
    pub shadows: u32,
    pub ao_resolution: u32,
    pub ao_strength: f32,
}

pub struct MainPass {
    pub main_pass: Pass,
    pub finalize_pass: Pass,
    pub lighting_pass: Pass,
    pub vars: Vars,

    camera_storage: Storage,
    var_storage: Storage,
    hashmap_storage: Storage,
    position_storage: Storage,
    color_storage: Storage,
}
impl MainPass {
    pub fn new(core: &Core, chunks: &ChunkManager) -> Self {
        let camera_storage = Storage::new(
            &core.device,
            32,
            true,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        let buffer_usages = wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::STORAGE;
        let size = (core.render_resolution[0] * core.render_resolution[1] * 16 + 8) as u64;
        let position_storage = Storage::new(&core.device, size, false, buffer_usages);
        let color_storage = Storage::new(&core.device, size, false, buffer_usages);

        core.queue.write_buffer(
            &position_storage.buffer,
            0,
            &[
                core.render_resolution[0].to_le_bytes(),
                core.render_resolution[1].to_le_bytes(),
            ]
            .concat(),
        );
        core.queue.write_buffer(
            &color_storage.buffer,
            0,
            &[
                core.render_resolution[0].to_le_bytes(),
                core.render_resolution[1].to_le_bytes(),
            ]
            .concat(),
        );

        let var_storage = Storage::new(
            &core.device,
            32,
            false,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        let hashmap_storage = Storage::new(
            &core.device,
            1024 * 100_000,
            false,
            wgpu::BufferUsages::STORAGE
                | wgpu::BufferUsages::COPY_DST
                | wgpu::BufferUsages::COPY_SRC,
        );

        let (main_pass, finalize_pass, lighting_pass) = get_passes(
            core,
            &hashmap_storage,
            &camera_storage,
            &var_storage,
            &position_storage,
            &color_storage,
            chunks,
        );

        let vars = Vars {
            time: 0.0,
            random_seed: 0,
            aspect_ratio: 0.0,
            lod_distance: 10.0,
            shadows: 1,
            ao_resolution: 6,
            ao_strength: 1.0,
        };

        Self {
            camera_storage,
            var_storage,
            main_pass,
            vars,
            hashmap_storage,
            position_storage,
            color_storage,
            finalize_pass,
            lighting_pass,
        }
    }

    pub fn update(&mut self, core: &Core, _dt: f32, camera: &Camera) {
        let aspect_ratio = core.size.width as f32 / core.size.height as f32;

        core.queue.write_buffer(
            &self.camera_storage.buffer,
            0,
            bytemuck::cast_slice(&[*camera]),
        );

        // clears to zero, mem is init?
        core.clear_buffer(&self.hashmap_storage.buffer);

        self.vars.random_seed += 1;
        self.vars.aspect_ratio = aspect_ratio;

        core.queue.write_buffer(
            &self.var_storage.buffer,
            0,
            &bytemuck::cast_slice(&[self.vars]),
        );
    }

    pub fn reload(&mut self, core: &Core, chunks: &ChunkManager) {
        let (main_pass, finalize_pass, lighting_pass) = get_passes(
            core,
            &self.hashmap_storage,
            &self.camera_storage,
            &self.var_storage,
            &self.position_storage,
            &self.color_storage,
            chunks,
        );

        self.main_pass = main_pass;
        self.finalize_pass = finalize_pass;
        self.lighting_pass = lighting_pass;
    }
}

fn get_passes(
    core: &Core,
    hashmap_storage: &Storage,
    camera_storage: &Storage,
    var_storage: &Storage,
    position_storage: &Storage,
    color_storage: &Storage,
    chunks: &ChunkManager,
) -> (Pass, Pass, Pass) {
    let main_shader = importer::shader_from_folder(
        "./shader/compute/",
        "main",
        &["noise", "ray", "aabb", "chunks", "hashmap"],
    );

    let g2_main = Group::get(
        &core.device,
        &[
            hashmap_storage,
            camera_storage,
            var_storage,
            position_storage,
            color_storage,
        ],
        wgpu::ShaderStages::COMPUTE,
    );

    let main_entry = Entry::from_groups(vec![chunks.get_group(core), g2_main]);

    let main_pass = Pass::new(core, main_entry, main_shader, "main");

    let finalize_shader =
        importer::shader_from_folder("./shader/compute/", "finalize", &["hashmap"]);
    let g1_finalize = Group::get(
        &core.device,
        &[
            hashmap_storage,
            position_storage,
            color_storage,
            &core.present_texture,
        ],
        wgpu::ShaderStages::COMPUTE,
    );

    let finalize_entry = Entry::from_groups(vec![chunks.get_group(core), g1_finalize]);
    let finalize_pass = Pass::new(core, finalize_entry, finalize_shader, "finalize");

    let lighting_shader =
        importer::shader_from_folder("./shader/compute/", "finalize", &["hashmap"]);
    let g1_lighting = Group::get(
        &core.device,
        &[
            hashmap_storage,
            position_storage,
            color_storage,
            &core.present_texture,
        ],
        wgpu::ShaderStages::COMPUTE,
    );

    let lighting_entry = Entry::from_groups(vec![chunks.get_group(core), g1_lighting]);
    let lighting_pass = Pass::new(core, lighting_entry, lighting_shader, "lighting");
    (main_pass, finalize_pass, lighting_pass)
}
