pub mod storage_texture;
pub use storage_texture::StorageTexture;

pub mod storage;
pub use storage::Storage;

pub mod texture_view;

pub trait EntryTrait {
    fn binding_type(&self) -> wgpu::BindingType;
    fn binding_resource(&self) -> wgpu::BindingResource;
}

// pub enum Entry {
//     StorageTexture(StorageTexture),
//     Storage(Storage),
// }
// impl EntryTrait for Entry {
//     fn binding_type(&self) -> wgpu::BindingType {
//         match self {
//             Self::StorageTexture(x) => x.binding_type(),
//             Self::Storage(x) => x.binding_type(),
//         }
//     }

//     fn binding_resource(&self) -> wgpu::BindingResource {
//         match self {
//             Self::StorageTexture(x) => x.binding_resource(),
//             Self::Storage(x) => x.binding_resource(),
//         }
//     }
// }

pub struct Group {
    // pub binding_type: wgpu::BindingType,
    // pub binding_resource: wgpu::BindingResource<'a>,
    pub layout: wgpu::BindGroupLayout,
    pub group: wgpu::BindGroup,
}
impl Group {
    pub fn get(
        device: &wgpu::Device,
        entries: &[&dyn EntryTrait],
        visibility: wgpu::ShaderStages,
    ) -> Self {
        let layout_entries: Vec<wgpu::BindGroupLayoutEntry> = entries
            .iter()
            .enumerate()
            .map(|(index, t)| wgpu::BindGroupLayoutEntry {
                binding: index as u32,
                visibility,
                ty: t.binding_type(),
                count: None,
            })
            .collect();

        let entries: Vec<wgpu::BindGroupEntry> = entries
            .iter()
            .enumerate()
            .map(|(index, t)| wgpu::BindGroupEntry {
                binding: index as u32,
                resource: t.binding_resource(),
            })
            .collect();

        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: None,
            entries: &layout_entries,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &bind_group_layout,
            entries: &entries,
        });

        Self {
            layout: bind_group_layout,
            group: bind_group,
        }
    }
}

pub struct Entry {
    pub groups: Vec<Group>,
}
impl Entry {
    pub fn from_groups(groups: Vec<Group>) -> Self {
        Self {
            groups
        }
    }
}
// impl<'a> Entry<'a> {
//     pub fn from<T: EntryTrait>(t: &'a T) -> Self {
//         Self {
//             binding_type: t.binding_type(),
//             binding_resource: t.binding_resource(),
//         }
//     }
// }

