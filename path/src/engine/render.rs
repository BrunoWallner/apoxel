use wgpu::util::DeviceExt;

use crate::app::GuiTrait;
use egui_wgpu::ScreenDescriptor;

use super::core::Core;
use super::entry::{Group, Storage};
use super::gui::EguiRenderer;

pub struct Render {
    pub egui: EguiRenderer,
    present_bind_group: wgpu::BindGroup,
    present_pipeline: wgpu::RenderPipeline,

    vars_bind_group: wgpu::BindGroup,
    vars_buffer: wgpu::Buffer,

    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
}
impl Render {
    pub fn new(core: &Core, egui: EguiRenderer) -> Self {
        // let (present_texture_bind_group_layout, present_texture_bind_group) =
        //     bindgroup::from_sampled_texture(device, position_texture, wgpu::ShaderStages::FRAGMENT);

        // present texture bind group
        let present_texture_view = core
            .present_texture
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let vars = Storage::new(
            &core.device,
            64,
            true,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        // let vars_buffer = vars.buffer;
        // let (vars_bind_group_layout, vars_bind_group) =
        //    get_binding(&core.device, &[&Entry::from(&vars)], wgpu::ShaderStages::VERTEX_FRAGMENT);
        let vars_group = Group::get(&core.device, &[&vars], wgpu::ShaderStages::VERTEX_FRAGMENT);
        let vars_buffer = vars.buffer;

        let present_bind_group_layout =
            core.device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    entries: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            sample_type: wgpu::TextureSampleType::Float { filterable: false },
                            view_dimension: wgpu::TextureViewDimension::D2,
                            multisampled: false,
                        },
                        count: None,
                    }],
                    label: Some("present_texture_bind_group_layout"),
                });

        let present_bind_group = core.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &present_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&present_texture_view),
            }],
            label: Some("present_bind_group"),
        });

        // // let present_shader = device.create_shader_module(wgpu::include_wgsl!("shader.wgsl"));
        // let present_shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
        //     label: Some("present shader"),
        //     source: wgpu::ShaderSource::Wgsl(include_str!("../shader/stage_2.wgsl").into()),
        // });

        let s = std::fs::read_to_string("./shader/render.wgsl").unwrap();

        let shader = core
            .device
            .create_shader_module(wgpu::ShaderModuleDescriptor {
                label: Some("stage 3 shader"),
                source: wgpu::ShaderSource::Wgsl(s.into()),
            });

        let present_pipeline_layout =
            core.device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("Present Pipeline"),
                    bind_group_layouts: &[&present_bind_group_layout, &vars_group.layout],
                    push_constant_ranges: &[],
                });

        let present_pipeline =
            core.device
                .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                    label: Some("Render Pipeline"),
                    layout: Some(&present_pipeline_layout),
                    vertex: wgpu::VertexState {
                        module: &shader,
                        entry_point: "vs_main",
                        buffers: &[mesh::Vertex::desc()],
                        compilation_options: wgpu::PipelineCompilationOptions::default(),
                    },
                    fragment: Some(wgpu::FragmentState {
                        module: &shader,
                        entry_point: "fs_main",
                        targets: &[Some(wgpu::ColorTargetState {
                            format: core.surface_format,
                            blend: Some(wgpu::BlendState::REPLACE),
                            write_mask: wgpu::ColorWrites::ALL,
                        })],
                        compilation_options: wgpu::PipelineCompilationOptions::default(),
                    }),
                    primitive: wgpu::PrimitiveState {
                        topology: wgpu::PrimitiveTopology::TriangleList,
                        strip_index_format: None,
                        front_face: wgpu::FrontFace::Ccw,
                        cull_mode: Some(wgpu::Face::Back),
                        polygon_mode: wgpu::PolygonMode::Fill,
                        unclipped_depth: false,
                        conservative: false,
                    },
                    depth_stencil: None,
                    multisample: wgpu::MultisampleState {
                        count: 1,
                        mask: !0,
                        alpha_to_coverage_enabled: false,
                    },
                    multiview: None,
                });

        let vertex_buffer = core
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(mesh::VERTICES),
                usage: wgpu::BufferUsages::VERTEX,
            });
        let index_buffer = core
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Index Buffer"),
                contents: bytemuck::cast_slice(mesh::INDICES),
                usage: wgpu::BufferUsages::INDEX,
            });

        Self {
            present_bind_group,
            present_pipeline,
            vertex_buffer,
            index_buffer,
            vars_bind_group: vars_group.group,
            vars_buffer,
            egui,
        }
    }

    pub fn present<F: GuiTrait>(
        &mut self,
        core: &Core,
        gui: &mut F,
    ) -> Result<(), wgpu::SurfaceError> {
        let output = core.surface.get_current_texture()?;

        let surface_size = output.texture.size();
        let serialized = [
            surface_size.width.to_le_bytes().to_vec(),
            surface_size.height.to_le_bytes().to_vec(),
        ]
        .concat();
        core.queue.write_buffer(&self.vars_buffer, 0, &serialized);

        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = core
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Present Encoder"),
            });

        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("Render Pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: 0.0,
                        g: 0.0,
                        b: 0.0,
                        a: 1.0,
                    }),
                    store: wgpu::StoreOp::Store,
                },
            })],
            depth_stencil_attachment: None,
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        render_pass.set_pipeline(&self.present_pipeline);
        render_pass.set_bind_group(0, &self.present_bind_group, &[]);
        render_pass.set_bind_group(1, &self.vars_bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
        render_pass.draw_indexed(0..6, 0, 0..1);

        drop(render_pass);

        let screen_descriptor = ScreenDescriptor {
            // size_in_pixels: [self.config.width, self.config.height],
            // size_in_pixels: [core.window.inner_size().width, core.window.inner_size().height],
            size_in_pixels: [core.size.width, core.size.height],
            pixels_per_point: core.window.scale_factor() as f32,
        };

        self.egui.draw(
            &core.device,
            &core.queue,
            &mut encoder,
            &core.window,
            &view,
            screen_descriptor,
            |ui| gui.render(ui),
        );

        // submit will accept anything that implements IntoIter
        core.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        Ok(())
    }
}

mod mesh {
    #[repr(C)]
    #[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
    pub struct Vertex {
        position: [f32; 3],
    }
    impl Vertex {
        pub fn desc() -> wgpu::VertexBufferLayout<'static> {
            wgpu::VertexBufferLayout {
                array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
                step_mode: wgpu::VertexStepMode::Vertex,
                attributes: &[wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                }],
            }
        }
    }

    pub const VERTICES: &[Vertex] = &[
        Vertex {
            position: [-1.0, -1.0, 0.0],
        },
        Vertex {
            position: [-1.0, 1.0, 0.0],
        },
        Vertex {
            position: [1.0, 1.0, 0.0],
        },
        Vertex {
            position: [1.0, -1.0, 0.0],
        },
    ];

    pub const INDICES: &[u32] = &[0, 2, 1, 2, 0, 3];
}
