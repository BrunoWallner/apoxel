use std::borrow::Cow;

use super::core::Core;
use super::entry::Entry;

pub struct Pass {
    entry_bind_groups: Vec<wgpu::BindGroup>,
    pipeline: wgpu::ComputePipeline,
}
impl Pass {
    pub fn new(core: &Core, entry: Entry, shader: wgpu::naga::Module, shader_entry: &str) -> Self {
        let mut bind_groups: Vec<wgpu::BindGroup> = Vec::with_capacity(entry.groups.len());
        let mut bind_group_layouts: Vec<wgpu::BindGroupLayout> =
            Vec::with_capacity(entry.groups.len());

        for group in entry.groups {
            // let group = Group::get(&core.device, group, wgpu::ShaderStages::COMPUTE);
            // let (bind_group_layout, bind_group) =
            //     get_binding(&core.device, &group[..].iter().collect::<Vec<_>>(), wgpu::ShaderStages::COMPUTE);
            bind_groups.push(group.group);
            bind_group_layouts.push(group.layout);
        }

        let compute_pipeline_layout =
            &core
                .device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("stage 1 pipeline layout"),
                    bind_group_layouts: &bind_group_layouts.iter().collect::<Vec<_>>(),
                    push_constant_ranges: &[],
                });

        let shader = &core
            .device
            .create_shader_module(wgpu::ShaderModuleDescriptor {
                label: Some("shader"),
                // source: wgpu::ShaderSource::Wgsl(s.into()),
                source: wgpu::ShaderSource::Naga(Cow::Owned(shader)),
            });

        let compute_pipeline =
            core.device
                .create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                    label: Some("compute pipeline"),
                    layout: Some(&compute_pipeline_layout),
                    module: &shader,
                    entry_point: shader_entry,
                    compilation_options: wgpu::PipelineCompilationOptions::default(),
                });

        Self {
            entry_bind_groups: bind_groups,
            pipeline: compute_pipeline,
        }
    }

    pub fn compute(&self, core: &Core, dispatch_size: [u32; 3]) {
        let mut encoder = core
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("stage 1 compute encoder"),
            });

        let mut compute_pass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
            label: Some("compute pass descriptor"),
            timestamp_writes: None,
        });

        compute_pass.set_pipeline(&self.pipeline);
        for (i, bind_group) in self.entry_bind_groups.iter().enumerate() {
            compute_pass.set_bind_group(i as u32, bind_group, &[]);
        }
        compute_pass.dispatch_workgroups(dispatch_size[0], dispatch_size[1], dispatch_size[2]);

        drop(compute_pass);

        core.queue.submit(std::iter::once(encoder.finish()));
    }
}
