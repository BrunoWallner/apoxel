mod serialize;
mod descriptor;

use super::core::Core;
use super::entry::{Group, Storage};
use common::{channel, chunk::Chunk, Coord};
use std::thread;

const DATA_SIZE: u64 = 2048_000_000;

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
struct ChunkVars {
    chunk_count: u32,
    data_size: u32,
}
impl Default for ChunkVars {
    fn default() -> Self {
        Self {
            chunk_count: 0,
            data_size: DATA_SIZE as u32,
        }
    }
}

pub struct ChunkManager {
    data_storage: Storage,
    var_storage: Storage,

    vars: ChunkVars,
    chunk_sender: channel::Sender<(Chunk, Coord)>,
    chunk_receiver: channel::Receiver<Vec<u8>>,
}
impl ChunkManager {
    pub fn new(device: &wgpu::Device) -> Self {
        let data_storage = super::entry::Storage::new(
            device,
            DATA_SIZE,
            false,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        let var_storage = Storage::new(
            device,
            32,
            true,
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        let (chunk_sender_tx, chunk_sender_rx) = channel::channel(None);
        let (chunk_receiver_tx, chunk_receiver_rx) = channel::channel(None);

        start_chunk_builder(chunk_sender_rx, chunk_receiver_tx);

        Self {
            data_storage,
            chunk_sender: chunk_sender_tx,
            chunk_receiver: chunk_receiver_rx,
            var_storage,
            vars: ChunkVars::default(),
        }
    }

    pub fn update(&mut self, core: &Core) {
        while let Ok(chunk) = self.chunk_receiver.try_recv() {
            // core.queue.write_buffer(&self.storage.buffer, 0, &1_u32.to_le_bytes());
            core.queue
                .write_buffer(&self.data_storage.buffer, 0, &chunk);
        }
        core.queue.write_buffer(
            &self.var_storage.buffer,
            0,
            bytemuck::cast_slice(&[self.vars]),
        );
    }

    pub fn push_chunk(&self, chunk: Chunk, coord: Coord) {
        if !chunk.is_empty() {
            self.chunk_sender.send((chunk, coord)).unwrap();
        } else {
            log::warn!("chunk is empty");
        }
    }

    pub fn get_group(&self, core: &Core) -> Group {
        Group::get(
            &core.device,
            &[&self.data_storage, &self.var_storage],
            wgpu::ShaderStages::COMPUTE,
        )
    }
}

fn start_chunk_builder(
    chunk_receiver: channel::Receiver<(Chunk, Coord)>,
    chunk_sender: channel::Sender<Vec<u8>>,
) {
    thread::spawn(move || loop {
        while let Some((chunk, coord)) = chunk_receiver.recv() {
            // log::info!("starting chunk serialization");
            let serialized = serialize::serialize_chunk(chunk, coord);
            log::info!("chunk mem: {} KB", serialized.len() / 1024);
            chunk_sender.send(serialized).unwrap();
        }
    });
}
