use glam::{Mat3, Vec3};
use std::ops::{Add, AddAssign, Sub, SubAssign};

pub struct CameraController {
    pub camera: Camera,
    pub impulse: Vec3,
    pub gravity: f32,
    // pub distances: Distances,
}
impl CameraController {
    pub fn new() -> Self {
        Self {
            camera: Camera::new(),
            impulse: Vec3::ZERO,
            gravity: 0.0,
            // distances: Distances::new(),
        }
    }

    pub fn movement(&mut self, direction: Vec3) {
        self.impulse += direction;
        let mut v: Vec3 = self.camera.position.into();
        v += self.impulse;
        self.camera.position = v.into();

        self.impulse *= 0.7;
        // self.impulse.y -= 0.0005;
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Camera {
    pub position: [f32; 3],
    pub yaw: f32,
    pub pitch: f32,
    pub fov: f32,

    _padding: [u32; 2],
}
impl Camera {
    pub fn new() -> Self {
        Self {
            position: [0.01; 3],
            yaw: 0.0,
            pitch: 0.0,
            fov: 0.9,
            _padding: [0; 2],
        }
    }

    pub fn get_rotation_matrix(&self) -> Mat3 {
        let yaw = Mat3::from_cols(
            Vec3::new(self.yaw.cos(), 0.0, self.yaw.sin()),
            Vec3::new(0.0, 1.0, 0.0),
            Vec3::new(-self.yaw.sin(), 0.0, self.yaw.cos()),
        );
        let pitch = Mat3::from_cols(
            Vec3::new(1.0, 0.0, 0.0),
            Vec3::new(0.0, self.pitch.cos(), -self.pitch.sin()),
            Vec3::new(0.0, self.pitch.sin(), self.pitch.cos()),
        );

        yaw * pitch
    }

    pub fn forward(&self) -> Vec3 {
        let rotation = self.get_rotation_matrix();
        rotation * Vec3::new(0.0, 0.0, 1.0)
    }

    pub fn right(&self) -> Vec3 {
        let rotation = self.get_rotation_matrix();
        rotation * Vec3::new(1.0, 0.0, 0.0)
    }
}

impl Add<Vec3> for Camera {
    type Output = Camera;

    fn add(self, rhs: Vec3) -> Self::Output {
        Camera {
            position: [
                self.position[0] + rhs.x,
                self.position[1] + rhs.y,
                self.position[2] + rhs.z,
            ],
            yaw: self.yaw,
            pitch: self.pitch,
            fov: 1.0,
            _padding: [0; 2],
        }
    }
}
impl Sub<Vec3> for Camera {
    type Output = Camera;

    fn sub(self, rhs: Vec3) -> Self::Output {
        Camera {
            position: [
                self.position[0] - rhs.x,
                self.position[1] - rhs.y,
                self.position[2] - rhs.z,
            ],
            yaw: self.yaw,
            pitch: self.pitch,
            fov: 1.0,
            _padding: [0; 2],
        }
    }
}

impl AddAssign<Vec3> for Camera {
    fn add_assign(&mut self, rhs: Vec3) {
        self.position[0] += rhs.x;
        self.position[1] += rhs.y;
        self.position[2] += rhs.z;
    }
}
impl SubAssign<Vec3> for Camera {
    fn sub_assign(&mut self, rhs: Vec3) {
        self.position[0] -= rhs.x;
        self.position[1] -= rhs.y;
        self.position[2] -= rhs.z;
    }
}
