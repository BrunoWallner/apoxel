use std::sync::{mpsc::channel, Arc};

use wgpu::Buffer;
use winit::{dpi::PhysicalSize, window::Window};

use super::entry::StorageTexture;

pub struct Core<'a> {
    pub window: Arc<Window>,
    pub size: PhysicalSize<u32>,
    pub render_resolution: [u32; 2],
    pub surface: wgpu::Surface<'a>,
    pub device: wgpu::Device,
    pub queue: wgpu::Queue,
    pub config: wgpu::SurfaceConfiguration,
    pub surface_format: wgpu::TextureFormat,
    pub present_texture: StorageTexture,
}
impl<'a> Core<'a> {
    pub async fn new(window: &Arc<Window>, render_resolution: [u32; 2]) -> Self {
        let window = window.clone();
        let size = window.inner_size();

        // The instance is a handle to our GPU
        // Backends::all => Vulkan + Metal + DX12 + Browser WebGPU
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            // backends: wgpu::Backends::all(),
            backends: wgpu::Backends::PRIMARY,
            // old slow and unmaintaned, no extra deps, fuck DX,
            dx12_shader_compiler: wgpu::Dx12Compiler::Fxc,
            // flags: wgpu::InstanceFlags::VALIDATION,
            flags: wgpu::InstanceFlags::from_build_config(),
            gles_minor_version: wgpu::Gles3MinorVersion::Automatic,
        });

        // # Safety
        //
        // The surface needs to live as long as the window that created it.
        // State owns the window so this should be safe.
        let surface = instance.create_surface(window.clone()).unwrap();

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    required_features: wgpu::Features::empty(),
                    // required_features: wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES,
                    // I WANT ALL THE MEMORY
                    required_limits: wgpu::Limits {
                        max_storage_buffer_binding_size: 2048_000_000,
                        max_buffer_size: 2048_000_000,
                        max_bind_groups: 8,
                        ..Default::default()
                    },
                    label: None,
                },
                None, // Trace path
            )
            .await
            .unwrap();

        let surface_caps = surface.get_capabilities(&adapter);

        let surface_format = surface_caps
            .formats
            .iter()
            .copied()
            .find(|f| !f.is_srgb())
            .unwrap_or(surface_caps.formats[0]);
        // let surface_format = wgpu::TextureFormat::Rgba8Unorm;
        let config = wgpu::SurfaceConfiguration {
            desired_maximum_frame_latency: 3,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::COPY_DST,
            format: surface_format,
            width: size.width,
            height: size.height,
            // present_mode: surface_caps.present_modes[0],
            present_mode: wgpu::PresentMode::AutoVsync,
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);


        let texture_usages = wgpu::TextureUsages::COPY_SRC
            | wgpu::TextureUsages::TEXTURE_BINDING
            | wgpu::TextureUsages::STORAGE_BINDING
            | wgpu::TextureUsages::COPY_DST;

        let present_texture = StorageTexture::new(
            &device,
            render_resolution,
            wgpu::TextureFormat::Rgba32Float,
            texture_usages,
            wgpu::StorageTextureAccess::WriteOnly,
        );
        
        Core {
            window,
            size,
            render_resolution,
            surface,
            device,
            queue,
            config,
            surface_format,
            present_texture,
        }
    }

    #[allow(dead_code)]
    pub fn read_from_buffer(&self, src_buffer: &Buffer, size: u64, clear: bool) -> Vec<u8> {
        // read output buffer to cpu
        let buffer = self.device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size,
            usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: None,
            });
        encoder.copy_buffer_to_buffer(src_buffer, 0, &buffer, 0, size);
        if clear {
            encoder.clear_buffer(src_buffer, 0, None);
        }
        self.queue.submit(std::iter::once(encoder.finish()));

        let (tx, rx) = channel();
        let slice = buffer.slice(..);
        slice.map_async(wgpu::MapMode::Read, move |result| {
            let _ = tx.send(result);
        });
        self.device.poll(wgpu::Maintain::Wait);
        rx.recv().unwrap().unwrap();

        let view = buffer.slice(..).get_mapped_range();
        let output = Vec::from(&view[0..size as usize]);
        output
    }

    #[allow(dead_code)]
    pub fn clear_buffer(&self, src_buffer: &Buffer) {
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: None,
            });
        encoder.clear_buffer(src_buffer, 0, None);
        self.queue.submit(std::iter::once(encoder.finish()));
    }
}
