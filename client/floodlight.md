# Floodlight
A Method to update chunk meshes with lighting across chunk-borders

## Structure
- the `Preprocessed` struct can be found at `chunks/preprocessor.rs`
- it consits of a block-  and light-data grid and 6 side-slices
  - the additional side-slices are there to improve accessibility for neighbours

## Instructions upon a chunk-update
1. build grid using chunk
2. recalculate lighting
3. generate external sides
4. update all neighbour chunks if associated side has changed
  - rebuild internal sides
  - recursively recalculate light
  - generate external sides
  - update all neighbour chunks if associated side has changed 

step 1 trough 3 should be in one function, while step 4 should be it's own
recursive function

The implementation can be found at `chunks/chunk_task.rs`