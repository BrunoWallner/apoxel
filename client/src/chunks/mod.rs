mod chunk_material;
mod chunk_task;
mod mesh;
mod preprocessor;

use bevy::prelude::*;
use chunk_material::ChunkMaterial;
pub use chunk_task::ChunkMeshSpawner;
pub use chunk_task::ChunkMeshTasker;

use common::Coord;
use std::collections::BTreeMap;

#[derive(Resource)]
struct ChunkEntityMap {
    pub map: BTreeMap<Coord, Entity>,
}
impl ChunkEntityMap {
    fn new() -> Self {
        Self {
            map: BTreeMap::default(),
        }
    }
}
pub struct ChunkPlugin;
impl Plugin for ChunkPlugin {
    fn build(&self, app: &mut App) {
        let chunk_mesh_spawner = ChunkMeshSpawner::init();
        let chunk_mesh_tasker = ChunkMeshTasker::init(&chunk_mesh_spawner);
        app.insert_resource(chunk_mesh_spawner)
            .add_plugin(MaterialPlugin::<ChunkMaterial>::default())
            .insert_resource(chunk_mesh_tasker)
            .insert_resource(ChunkEntityMap::new())
            .add_system(chunk_spawn_animation)
            .add_system(ChunkMeshSpawner::run);
    }
}

const ANIMATION_DURATION: f32 = 0.5; // in secs
const SPAWN_ORIGIN_OFFSET: f32 = 2.0; // in chunks

#[derive(Component)]
pub struct ChunkSpawnAnimation {
    pub height: f32,
    pub time: f32,
}

#[allow(clippy::type_complexity)]
fn chunk_spawn_animation(
    mut commands: Commands,
    mut set: ParamSet<(
        Query<(
            Entity,
            &Visibility,
            &mut Transform,
            &mut ChunkSpawnAnimation,
        )>,
        Query<(&mut Transform, &ChunkSpawnAnimation), Added<ChunkSpawnAnimation>>,
    )>,
    time: Res<Time>,
) {
    for (mut transform, animation) in set.p1().iter_mut() {
        transform.translation.y = animation.height - SPAWN_ORIGIN_OFFSET;
    }
    for (entity, visibility, mut transform, mut animation) in set.p0().iter_mut() {
        if visibility == Visibility::Visible {
            animation.time += (time.delta().as_millis() as f32) / 1_000.0;
            let relative = (animation.time / ANIMATION_DURATION).clamp(0.0, 1.0);
            let relative = dampen(relative);
            let mut height = ((animation.height - SPAWN_ORIGIN_OFFSET) * (1.0 - relative))
                + (animation.height * relative);
            // clamp and remove animation when finished
            if height >= animation.height {
                commands.entity(entity).remove::<ChunkSpawnAnimation>();
                height = animation.height;
            }
            transform.translation.y = height;
        }
    }
}

fn dampen(x: f32) -> f32 {
    6.0 * x.powi(5) - 15.0 * x.powi(4) + 10.0 * x.powi(3)
}
