mod ao;
mod sides;

use bevy::prelude::*;
use bevy::render::mesh::Indices;
use bevy::render::render_resource::PrimitiveTopology;
use common::prelude::BASE_CHUNK_SIZE;

use super::chunk_material::ATTRIBUTE_COLOR;
use super::chunk_material::ATTRIBUTE_LIGHT;
use super::chunk_material::ATTRIBUTE_NORMAL;

use common::Coord;

use super::preprocessor::Preprocessed;
use common::types::{octree::Neighbour, octree::Side};

/*
000
100
010
110
001
101
011
111
*/

pub fn empty() -> Mesh {
    let v_length = 1000; // IDK, seems like a good number
    let mut positions: Vec<[f32; 3]> = Vec::with_capacity(v_length);
    let mut normals: Vec<[f32; 4]> = Vec::with_capacity(v_length);
    let mut colors: Vec<[f32; 4]> = Vec::with_capacity(v_length);
    let mut lights: Vec<f32> = Vec::with_capacity(v_length);
    let mut uvs: Vec<[f32; 2]> = Vec::with_capacity(v_length);
    let mut indices: Vec<u32> = Vec::with_capacity(v_length);

    for side in Side::all() {
        sides::push([0.0, 0.0, 0.0], 1.0, &mut positions, &mut indices, side);
        for _ in 0..4 {
            lights.push(0.0);
            uvs.push([0.0, 0.0]);
            normals.push([0.0, 0.0, 0.0, 0.0]);
            colors.push([0.0, 0.0, 0.0, 0.0]);
        }
    }

    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh.insert_attribute(ATTRIBUTE_NORMAL, normals);
    mesh.insert_attribute(ATTRIBUTE_COLOR, colors);
    mesh.insert_attribute(ATTRIBUTE_LIGHT, lights);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
    mesh.set_indices(Some(Indices::U32(indices)));

    mesh
}

// around 9ms
pub fn generate(chunk: &Preprocessed, coord: &Coord) -> Mesh {
    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    let v_length = 1024; // IDK, seems like a good number

    let mut positions: Vec<[f32; 3]> = Vec::with_capacity(v_length);
    let mut normals: Vec<[f32; 4]> = Vec::with_capacity(v_length);
    let mut colors: Vec<[f32; 4]> = Vec::with_capacity(v_length);
    let mut lights: Vec<f32> = Vec::with_capacity(v_length);
    let mut uvs: Vec<[f32; 2]> = Vec::with_capacity(v_length);

    let mut indices: Vec<u32> = Vec::with_capacity(v_length);
    let block_size = 1.0 / BASE_CHUNK_SIZE as f32;

    let multidimensional_iterator = (0..BASE_CHUNK_SIZE).flat_map(move |x| {
        (0..BASE_CHUNK_SIZE).flat_map(move |y| (0..BASE_CHUNK_SIZE).map(move |z| (x, y, z)))
    });

    for (x, y, z) in multidimensional_iterator {
        let relative_position = [
            x as f32 / BASE_CHUNK_SIZE as f32,
            y as f32 / BASE_CHUNK_SIZE as f32,
            z as f32 / BASE_CHUNK_SIZE as f32,
        ];
        // offset of 1 to not underflow in later neighbour offset
        let (x, y, z) = (x as usize + 1, y as usize + 1, z as usize + 1);
        let block = chunk.get_block(x, y, z).unwrap();
        // if x == 1 || y == 1 || z == 1 {
        // light = Color::splat(255);
        // }

        if !block.has_mass() {
            continue;
        }

        // get neighbours for meshing and ao
        let mut neighbours: [bool; 26] = [false; 26];
        for n in Neighbour::all() {
            let offset = n.get_offset();
            let (x_offset, y_offset, z_offset) = (
                x as isize + offset[0] as isize,
                y as isize + offset[1] as isize,
                z as isize + offset[2] as isize,
            );
            if chunk
                .get_block(x_offset as usize, y_offset as usize, z_offset as usize)
                .unwrap()
                .has_mass()
            {
                neighbours[n as usize] = true;
            }
        }
        // push sides
        for side in Side::all() {
            if !neighbours[Neighbour::from_side(&side) as usize] {
                sides::push(
                    relative_position,
                    block_size,
                    &mut positions,
                    &mut indices,
                    side,
                );

                ao::push(side, &neighbours, &mut lights);

                // rest of temporary garbage
                let color = block.to_color();
                let color = [
                    color.r as f32 / 255.0,
                    color.g as f32 / 255.0,
                    color.b as f32 / 255.0,
                    1.0,
                ];

                let global_pos = [
                    coord[0] as f32 + relative_position[0],
                    coord[1] as f32 + relative_position[1],
                    coord[2] as f32 + relative_position[2],
                ];
                for _ in 0..4 {
                    uvs.push([0.0, 0.0]);
                    normals.push([global_pos[0], global_pos[1], global_pos[2], 0.0]);
                    colors.push(color);
                }
            }
        }
    }

    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh.insert_attribute(ATTRIBUTE_NORMAL, normals);
    mesh.insert_attribute(ATTRIBUTE_COLOR, colors);
    mesh.insert_attribute(ATTRIBUTE_LIGHT, lights);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);

    mesh.set_indices(Some(Indices::U32(indices)));

    mesh
}

// // takes ~ 150 ms for 32x32x32 chunk
// // awfull
// pub fn generate(chunk: &Chunk, coord: &Coord) -> Mesh {
//     let out = chunk.traverse(Some(12));

//     let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
//     let v_length = 1000; // IDK, seems like a good number

//     let mut positions: Vec<[f32; 3]> = Vec::with_capacity(v_length);
//     let mut normals: Vec<[f32; 4]> = Vec::with_capacity(v_length);
//     let mut colors: Vec<[f32; 4]> = Vec::with_capacity(v_length);
//     let mut lights: Vec<f32> = Vec::with_capacity(v_length);
//     let mut uvs: Vec<[f32; 2]> = Vec::with_capacity(v_length);

//     let mut indices: Vec<u32> = Vec::with_capacity(v_length);

//     for out in out.1 {
//         // let rel_depth = max_depth as f32 - out.depth as f32;
//         let (block, depth) = match out.node.to_end() {
//             (Some(block), layer) => (block, layer),
//             (None, layer) => (Block::None, layer),
//         };
//         if block.has_mass() {
//             let depth = out.depth + depth;
//             let size = 1.0 / 2_f32.powf(out.depth as f32);
//             let path_size = Octree::get_size_from_depth(out.depth) as f32;
//             let position = out.position;

//             let size_3 = [size, size, size];

//             let global_pos = [
//                 position[0] + coord[0] as f32,
//                 position[1] + coord[1] as f32,
//                 position[2] + coord[2] as f32,
//             ];

//             // get neighbours for efficient meshing and AO
//             let mut neighbours: [bool; 26] = [false; 26];
//             for n in Neighbour::all() {
//                 let neighbour = chunk.get_neighbours(
//                     &n,
//                     path_size as u32,
//                     (position[0] * path_size) as u32,
//                     (position[1] * path_size) as u32,
//                     (position[2] * path_size) as u32,
//                 );
//                 // check if side should render
//                 let should_render = if let Some(neighbour) = neighbour {
//                     let mut full_transparent: bool = true;

//                     for n in neighbour {
//                         let block = match n.node.to_end().0 {
//                             Some(block) => block,
//                             None => Block::None,
//                         };
//                         if block.has_mass() && n.depth <= depth {
//                             full_transparent = false;
//                             break;
//                         }
//                     }

//                     full_transparent
//                 } else {
//                     true
//                 };
//                 neighbours[n as usize] = should_render;
//             }

//             for side in Side::all() {
//                 let should_render = neighbours[Neighbour::from_side(&side) as usize];
//                 if !should_render {
//                     continue;
//                 }

//                 sides::push(position, size_3, &mut positions, &mut indices, side);

//                 // ambient occlusion
//                 ao::push(&side, &neighbours, &mut lights);

//                 for _ in 0..4 {
//                     uvs.push([0.0, 0.0]);
//                     normals.push([
//                         global_pos[0],
//                         global_pos[1],
//                         global_pos[2],
//                         out.depth as f32,
//                     ]);
//                 }

//                 let color = block.to_color();
//                 for _ in 0..4 {
//                     let color = [
//                         color.r as f32 / 255.0,
//                         color.g as f32 / 255.0,
//                         color.b as f32 / 255.0,
//                         1.0,
//                     ];
//                     colors.push(color);
//                 }
//             }
//         }
//     }

//     mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
//     mesh.insert_attribute(ATTRIBUTE_NORMAL, normals);
//     mesh.insert_attribute(ATTRIBUTE_COLOR, colors);
//     mesh.insert_attribute(ATTRIBUTE_LIGHT, lights);
//     mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);

//     mesh.set_indices(Some(Indices::U32(indices)));

//     mesh
// }
