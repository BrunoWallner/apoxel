use common::types::octree::Side;

const SIDES: [[[u8; 3]; 4]; 6] = [
    // left
    [[0, 0, 0], [0, 1, 0], [0, 0, 1], [0, 1, 1]],
    // right
    [[1, 0, 0], [1, 1, 0], [1, 0, 1], [1, 1, 1]],
    // back
    [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0]],
    // front
    [[0, 0, 1], [1, 0, 1], [0, 1, 1], [1, 1, 1]],
    // top
    [[0, 1, 0], [1, 1, 0], [0, 1, 1], [1, 1, 1]],
    // bottom
    [[0, 0, 0], [1, 0, 0], [0, 0, 1], [1, 0, 1]],
];

const INDICES: [[u32; 6]; 6] = [
    // left
    [0, 3, 1, 0, 2, 3],
    // right
    [0, 1, 3, 0, 3, 2],
    // back
    [0, 2, 3, 0, 3, 1],
    // front
    [0, 3, 2, 0, 1, 3],
    //top
    [2, 1, 0, 2, 3, 1],
    //bottom
    [2, 0, 1, 2, 1, 3],
];

pub fn push(
    position: [f32; 3],
    size: f32,

    positions: &mut Vec<[f32; 3]>,
    indices: &mut Vec<u32>,

    side: Side,
) {
    let index_offset = positions.len() as u32;
    for v in SIDES[side as usize] {
        positions.push([
            (position[0] + (v[0] as f32 * size)),
            (position[1] + (v[1] as f32 * size)),
            (position[2] + (v[2] as f32 * size)),
        ]);
    }
    for i in INDICES[side as usize] {
        indices.push(i + index_offset);
    }
}
