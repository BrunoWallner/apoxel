pub mod camera;
mod plugin;

use bevy::prelude::Component;
pub use plugin::PlayerPlugin;

#[derive(Component)]
pub struct UserRaycastable;

#[derive(Component)]
pub struct Player {
    pub position_synced: bool,
}
impl Player {
    pub fn new() -> Self {
        Self {
            position_synced: false,
        }
    }
}
