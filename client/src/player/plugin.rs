use super::camera::{CameraPlugin, FlyCamera};
use super::Player;
use crate::connection::Communicator;
use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::prelude::*;
use common::event::prelude::*;
use common::prelude::{get_chunk_coords, Block, Octree, SuperChunk, BASE_CHUNK_SIZE};

use rand::Rng;

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_player)
            .add_system(sync_position)
            .add_system(place)
            .add_plugin(CameraPlugin);
    }
}

fn spawn_player(mut commands: Commands) {
    commands
        .spawn(Camera3dBundle {
            projection: bevy::render::camera::Projection::Perspective(PerspectiveProjection {
                fov: 1.5,
                near: 0.00001,
                far: 100.0,
                ..default()
            }),
            camera: Camera {
                hdr: true,
                ..default()
            },
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::Custom(Color::Rgba {
                    red: 0.0,
                    green: 0.0,
                    blue: 0.0,
                    alpha: 1.0,
                }),
                ..default()
            },
            ..default()
        })
        .insert(Player::new())
        .insert(FlyCamera);
    // .insert(RayCastSource::<UserRaycastable>::new());
}

fn place(
    communicator: Option<Res<Communicator>>,
    player: Query<&Transform, With<FlyCamera>>,
    mouse: Res<Input<MouseButton>>,
) {
    let Some(communicator) = communicator else {return};
    let Some(transform) = player.iter().next() else {return};
    if mouse.just_pressed(MouseButton::Right) {
        let chunk: Octree<Block> = Octree::default();
        let coord = transform.translation * BASE_CHUNK_SIZE as f32;
        let coord = [coord.x as i64, coord.y as i64, coord.z as i64];
        let (chunk_coord, block_coord) = get_chunk_coords(BASE_CHUNK_SIZE, &coord);
        let mut structure = SuperChunk::new(chunk, chunk_coord);
        let block_coord = [
            block_coord[0] as i64,
            block_coord[1] as i64,
            block_coord[2] as i64,
        ];

        let mut rng = rand::thread_rng();
        let r: u8 = rng.gen_range(50..255);
        let g: u8 = rng.gen_range(50..255);
        let b: u8 = rng.gen_range(50..255);

        structure.place(
            BASE_CHUNK_SIZE,
            block_coord,
            Block::Light(common::color::Color::new(r, g, b)),
            // Block::Color(common::color::Color::new(100, 100, 100)),
        );
        communicator.send_event(CTS::PlaceStructure { structure })
    }

    if mouse.just_pressed(MouseButton::Left) {
        let chunk: Octree<Block> = Octree::default();
        let coord = transform.translation * BASE_CHUNK_SIZE as f32;
        let coord = [coord.x as i64, coord.y as i64, coord.z as i64];
        let (chunk_coord, block_coord) = get_chunk_coords(BASE_CHUNK_SIZE, &coord);
        let mut structure = SuperChunk::new(chunk, chunk_coord);
        let block_coord = [
            block_coord[0] as i64,
            block_coord[1] as i64,
            block_coord[2] as i64,
        ];
        structure.place(
            BASE_CHUNK_SIZE,
            block_coord,
            Block::Water,
            // Block::Color(common::color::Color::new(100, 100, 100)),
        );
        communicator.send_event(CTS::PlaceStructure { structure })
    }
}

fn sync_position(
    communicator: Option<Res<Communicator>>,
    player: Query<(&Transform, &Player), With<FlyCamera>>,
) {
    if let Some(communicator) = communicator {
        if let Some((transform, player)) = player.iter().next() {
            if player.position_synced {
                let translation = transform.translation;
                let coord = [
                    translation.x as f64,
                    translation.y as f64,
                    translation.z as f64,
                ];
                communicator.send_event(Move { coord });
            }
        }
    }
}
