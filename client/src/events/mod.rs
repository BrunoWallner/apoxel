mod handler;

use bevy::prelude::*;

pub struct EventHandlePlugin;

impl Plugin for EventHandlePlugin {
    fn build(&self, app: &mut App) {
        app.add_system(handler::handle);
    }
}
