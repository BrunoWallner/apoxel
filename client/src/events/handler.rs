use crate::chunks::ChunkMeshTasker;
use crate::connection::Communicator;
use bevy::prelude::*;
use common::event::prelude::*;

pub(super) fn handle(
    communicator: Option<ResMut<Communicator>>,
    chunk_mesh_tasker: Res<ChunkMeshTasker>,
) {
    if let Some(mut communicator) = communicator {
        while let Ok(event) = communicator.try_get_event() {
            match event {
                Error(e) => {
                    log::warn!("got error from server: {:?}", e);
                }
                ChunkUpdates(updates) => {
                    for (chunk, coord) in updates {
                        if !chunk.is_empty() {
                            let _ = chunk_mesh_tasker.task(chunk, coord);
                        }
                    }
                }
                _ => (),
            }
        }
    }
}
