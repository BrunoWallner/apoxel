mod connect;
mod load;

use bevy::prelude::*;

const PLAY_BUTTON_COLOR: Color = Color::rgb(0.15, 0.15, 0.15);
const PLAY_BUTTON_HOVER_COLOR: Color = Color::rgb(0.25, 0.25, 0.25);
const PLAY_BUTTON_PRESSED_COLOR: Color = Color::rgb(0.35, 0.75, 0.35);

#[derive(Resource, Component, PartialEq)]
pub enum MenuState {
    InMenu,
    Connecting,
    Connected,
}

pub struct MainMenuPlugin;
impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(MenuState::InMenu)
            .add_startup_system(setup)
            .add_system(play_button_system)
            .add_system(connect::connect)
            .add_system(load::load);
    }
}

#[derive(Component)]
struct Menu;

#[derive(Component)]
struct PlayButton;

#[derive(Component)]
struct ConnectionStatus;

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    // // ui camera
    // commands.spawn_bundle(Camera2dBundle::default())
    //     .insert(Menu);

    // play button
    commands
        .spawn(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(350.0), Val::Px(65.0)),
                // center button
                margin: UiRect::all(Val::Auto),
                // horizontally center child text
                justify_content: JustifyContent::Center,
                // vertically center child text
                align_items: AlignItems::Center,
                ..default()
            },
            background_color: PLAY_BUTTON_COLOR.into(),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "Play",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                },
            ));
        })
        .insert(PlayButton)
        .insert(Menu);

    // connection status
    commands
        .spawn(
            // Create a TextBundle that has a Text with a single section.
            TextBundle::from_section(
                // Accepts a `String` or any type that converts into a `String`, such as `&str`
                "",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 30.0,
                    color: Color::ORANGE,
                },
            ) // Set the alignment of the Text
            .with_text_alignment(TextAlignment::Center)
            // Set the style of the TextBundle itself.
            .with_style(Style {
                margin: UiRect::all(Val::Auto),
                position_type: PositionType::Absolute,
                position: UiRect {
                    bottom: Val::Px(10.0),
                    left: Val::Px(10.0),
                    ..default()
                },
                ..default()
            }),
        )
        .insert(ConnectionStatus)
        .insert(Menu);
}

#[allow(clippy::type_complexity)]
fn play_button_system(
    mut interaction_query: Query<
        (&Interaction, &mut BackgroundColor, &Children),
        (Changed<Interaction>, With<Button>, With<PlayButton>),
    >,
    mut menu_state: ResMut<MenuState>,
    mut text_query: Query<&mut Text>,
) {
    for (interaction, mut color, children) in &mut interaction_query {
        let mut text = text_query.get_mut(children[0]).unwrap();
        match *interaction {
            Interaction::Clicked => {
                text.sections[0].value = "LETS FUCKING GOOO".to_string();
                *color = PLAY_BUTTON_PRESSED_COLOR.into();
                *menu_state = MenuState::Connecting;
            }
            Interaction::Hovered => {
                *color = PLAY_BUTTON_HOVER_COLOR.into();
            }
            Interaction::None => {
                *color = PLAY_BUTTON_COLOR.into();
            }
        }
    }
}
