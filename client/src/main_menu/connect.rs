use bevy::prelude::*;

use crate::connection::Communicator;
use crate::player::Player;
use common::event::prelude::*;
use common::Token;
use common::User;


use super::{ConnectionStatus, MenuState};

pub(super) fn connect(
    mut commands: Commands,
    mut menu_state: ResMut<MenuState>,
    mut connection_status: Query<&mut Text, With<ConnectionStatus>>,
    mut player: Query<(&mut Player, &mut Transform)>,
) {
    if *menu_state == MenuState::Connecting {
        if let Some(mut connection_status) = connection_status.iter_mut().next() {
            connection_status.sections[0].value = String::from("connecting ...");
            if let Some(mut communicator) = Communicator::init("localhost:8000") {
                connection_status.sections[0].value = String::from("connected");
                *menu_state = MenuState::Connected;
                let mut token: Option<Token> = None;
                // log in when token is available, or register
                if let Some(t) = storage::token::get() {
                    token = Some(t);
                } else {
                    communicator.send_event(CTS::Register {
                        name: String::from("user"),
                    });
                    // handle incoming events from server to get token
                    'get: while let Ok(event) = communicator.get_event() {
                        match event {
                            Token(t) => {
                                token = Some(t);
                                break 'get;
                            }
                            Error(e) => {
                                token = None;
                                log::warn!("{:?}", e);
                                connection_status.sections[0].value = format!("{e:?}");
                                break 'get;
                            }
                            ev => log::warn!("received unexpected event at login: {:?}", ev),
                        }
                    }
                }
                log::info!("got token, logging in...");
                if let Some(token) = token {
                    // login with aquired token
                    communicator.set_token(token);
                    communicator.send_event(CTS::Login {
                        token: communicator.token,
                    });

                    // handle incoming events to check if login was successfull
                    let mut logged_in: bool = false;
                    let mut user: Option<User> = None;
                    if let Ok(event) = communicator.get_event() {
                        match event {
                            LoggedIn(pi) => {
                                user = Some(pi);
                                logged_in = true
                            }
                            Error(e) => match e {
                                ClientError::Login(l) => match l {
                                    LoginError::AlreadyLoggedIn => {
                                        logged_in = false;
                                        log::warn!("user already logged in");
                                        connection_status.sections[0].value =
                                            String::from("user already logged in elsewhere");
                                        *menu_state = MenuState::InMenu;
                                    }
                                    LoginError::InvalidToken => {
                                        logged_in = false;
                                        log::warn!("invalid login token");
                                        connection_status.sections[0].value =
                                            String::from("invalid login token, try again");
                                        *menu_state = MenuState::InMenu;
                                        let _ = storage::token::remove();
                                    }
                                },
                                e => {
                                    log::warn!("received unexpected event at login: {:?}", e);
                                }
                            },
                            ev => {
                                log::warn!("received unexpected event at login: {:?}", ev);
                            }
                        }
                    }

                    if logged_in {
                        log::info!("logged in");
                        storage::token::set(&communicator.token).unwrap();
                        connection_status.sections[0].value = String::from("logged in");

                        commands.insert_resource(communicator);

                        // sync player_information
                        if let Some(user) = user {
                            for (mut player, mut transform) in player.iter_mut() {
                                player.position_synced = true;
                                let coord = user.pos;
                                transform.translation = Vec3 {
                                    x: coord[0] as f32,
                                    y: coord[1] as f32,
                                    z: coord[2] as f32,
                                };
                            }
                        }
                    }
                } else {
                    log::warn!("failed to get token");
                    connection_status.sections[0].value = String::from("failed to get token!");
                    *menu_state = MenuState::InMenu;
                }
            } else {
                connection_status.sections[0].value =
                    String::from("failed to connect, please try again later");
                *menu_state = MenuState::InMenu;
            }
        }
    }
}
