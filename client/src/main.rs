mod chunks;
mod connection;
mod events;
mod main_menu;
mod player;

use bevy::prelude::*;

fn main() {
    async_std::task::spawn(async move {
        server::start().await.unwrap();
    });

    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(main_menu::MainMenuPlugin)
        .add_plugin(events::EventHandlePlugin)
        .add_plugin(player::PlayerPlugin)
        .add_plugin(chunks::ChunkPlugin)
        .run();
}
